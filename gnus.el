;; Gnus config file
;; - Guillermo Navarro

(setq user-full-name "Guillermo Navarro-Arribas")

;; set gnus as default for handling mail in Emacs
(setq mail-user-agent 'gnus-user-agent)
(setq read-mail-command 'gnus)

;; Do not check new groups
(setq gnus-check-new-newsgroups nil)

;; conect directly to dovecot local imap server
(setq imap-shell-program 
      "/usr/lib/dovecot/imap -c ~/.dovecotrc")

;; retrieve mail from local IMAP server
(setq gnus-select-method
     '(nnimap "Mail"
              (nnimap-stream shell)))

;; SMTP
(require 'smtpmail)

;; smtp debug info
;; (setq smtpmail-debug-info t)
;; (setq smtpmail-debug-verb t)

(setq message-send-mail-function 'smtpmail-send-it)

(defun g-set-smtp-parameters (server port user &optional password)
  "Sets the main parameters for the smtp server over SSL"
  (setq smtpmail-starttls-credentials `((,server ,port nil nil)))
  ;; (setq smtpmail-auth-credentials `((,server ,port ,user ,password)))
  ;; (setq smtpmail-default-smtp-server server)
  (setq smtpmail-smtp-server server)
  (setq smtpmail-smtp-user user)
  (setq smtpmail-smtp-service port)
  (message "Usign smpt server: %s" server))

(defstruct gsmtp server port username (password nil))

(setq g-smtp-servers 
      `(("default" .
         ,(make-gsmtp :server "smtp.gmail.com"
                      :port 587
                      :username "gna.mail.11"))
        ("guillermo.navarro@gmail.com" .
         ,(make-gsmtp :server "smtp.gmail.com"
                      :port 587
                      :username "guillermo.navarro"))
        ("guille@iiia.csic.es" .
         ,(make-gsmtp :server "mail.iiia.csic.es"
                      :port 587
                      :username "guille"))
        ("guillermo.navarro@uab.cat" .
         ,(make-gsmtp :server "deic.uab.cat"
                      :port 465
                      :username "guille"))
        ("gnavarro@deic.uab.cat" .
         ,(make-gsmtp :server "deic.uab.cat"
                      :port 465
                      :username "guille"))
        ))

(defun assoc-string-match (key alist &optional ignore)
  "Assoc using string-match as equal function."
  (assoc-default key alist 'string-match))

(defun g-set-smtp-server ()
  "Sets the smtp server based on the from field of the message"
  (save-excursion
    (let* ((from (save-restriction
                   (message-narrow-to-headers)
                   (message-fetch-field "from")))
           (server (assoc-string-match from g-smtp-servers t)))
      (unless server (setq server (cdr (assoc "default" g-smtp-servers))))
      (message "Setting SMTP: %s for user: %s" server from)
      (g-set-smtp-parameters (gsmtp-server server)
                             (gsmtp-port server)
                             (gsmtp-username server)))))

(add-hook 'message-send-hook 'g-set-smtp-server)


;; move articles to gmail's trash
(defun g-gnus-move-article-to-Trash (&optional n)
  (interactive "P")
  (gnus-summary-move-article n "[Gmail].Trash")
  (unless (gnus-summary-last-article-p (gnus-summary-article-number))
    (gnus-summary-next-article n))
  (gnus-summary-position-point))

;; move articles to gmail's spam
(defun g-gnus-move-article-to-Spam (&optional n)
  (interactive "P")
  (gnus-summary-move-article n "[Gmail].Spam")
  (unless (gnus-summary-last-article-p (gnus-summary-article-number))
    (gnus-summary-next-article n))
  (gnus-summary-position-point))

(defun g-gnus-summary-keys ()
   (local-set-key (kbd "y") 'g-gnus-move-article-to-Trash)
   (local-set-key (kbd "$") 'g-gnus-move-article-to-Spam))
(add-hook 'gnus-summary-mode-hook 'g-gnus-summary-keys)

;; set the "from" addres from the "to" when replaying for my addresses
(setq message-alternative-emails
      (regexp-opt '("guillermo.navarro@uab.es"
                    "guillermo.navarro@uab.cat"
                    "gnavarro@deic.uab.es"
                    "gnavarro@deic.uab.cat"
                    "guille@deic.uab.es"
                    "guille@deic.uab.cat"
                    "guille@iiia.csic.es"
                    "guillermo.navarro@gmail.com"
                    "guillermo.navarro.arribas@gmail.com"
                    "guilledist@gmail.com"
                    "gnavarroar@uoc.edu"
                    )))

;; gnus-alias: set alias for sending mail
(when (require 'gnus-alias nil t)
  (gnus-alias-init)
  (setq gnus-alias-identity-alist 
        '(("uab" nil "Guillermo Navarro-Arribas <guillermo.navarro@uab.cat>"
           "DEIC - UAB" nil nil nil)
          ("deic" nil "Guillermo Navarro-Arribas <gnavarro@deic.uab.cat>"
           "DEIC - UAB" nil nil nil)
          ("iiia" nil "Guillermo Navarro-Arribas <guille@iiia.csic.es>"
           "IIIA - CSIC" nil nil nil)
          ("personal" nil
           "Guillermo Navarro-Arribas <guillermo.navarro@gmail.com>"
           nil nil nil nil)))
  (setq gnus-alias-default-identity "uab")
  (define-key message-mode-map (kbd "C-c i") 'gnus-alias-select-identity))

;; don't display smileys
(setq gnus-treat-display-smileys nil)

;; default mime type for X m
(setq gnus-summary-save-parts-default-mime ".*/.*")

