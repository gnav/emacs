(eval-when-compile    (require 'color-theme))
(defun color-theme-jao-osx ()
  "Color theme by Jose Ortega, created 2008-01-02."
  (interactive)
  (color-theme-install
   '(color-theme-jao-osx
     ((background-color . "gainsboro")
      (background-mode . light)
      (border-color . "black")
      (cursor-color . "black")
      (foreground-color . "black")
      (mouse-color . "black"))
     ((compilation-message-face . underline)
      (gnus-article-button-face . bold)
      (gnus-article-mouse-face . highlight)
      (gnus-cite-attribution-face . gnus-cite-attribution)
      (gnus-mouse-face . highlight)
      (gnus-signature-face . gnus-signature)
      (gnus-summary-selected-face . gnus-summary-selected)
      (gnus-treat-display-face . head)
      (ispell-highlight-face . flyspell-incorrect)
      (list-matching-lines-buffer-name-face . underline)
      (list-matching-lines-face . match)
      (rmail-highlight-face . rmail-highlight)
      (snippet-bound-face . bold)
      (snippet-field-face . highlight)
      (term-default-bg-color . "gainsboro")
      (term-default-fg-color . "black")
      (view-highlight-face . highlight)
      (w3m-form-mouse-face . highlight)
      (widget-mouse-face . highlight))
     (default ((t (:stipple nil :background "gainsboro" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :family "apple-monaco"))))
     (bbdb-company ((t (:italic t :slant italic))))
     (bbdb-field-name ((t (:bold t :weight bold))))
     (bbdb-field-value ((t (nil))))
     (bbdb-name ((t (:underline t))))
     (bmk-mgr-bookmark-face ((t (nil))))
     (bmk-mgr-folder-face ((t (:bold t :weight bold))))
     (bmk-mgr-sel-bookmark-face ((t (:foreground "IndianRed"))))
     (bmk-mgr-sel-folder-face ((t (:bold t :foreground "IndianRed" :weight bold))))
     (bold ((t (:bold t :weight bold))))
     (bold-italic ((t (:italic t :bold t :slant italic :weight bold))))
     (border ((t (:background "black"))))
     (buffer-menu-buffer ((t (:bold t :weight bold))))
     (button ((t (:underline t))))
     (c-nonbreakable-space-face ((t (:bold t :background "red" :foreground "black" :weight bold))))
     (change-log-acknowledgement ((t (:foreground "grey40"))))
     (change-log-conditionals ((t (:foreground "DodgerBlue4"))))
     (change-log-date ((t (:foreground "darkseagreen4"))))
     (change-log-email ((t (:foreground "DodgerBlue4"))))
     (change-log-file ((t (:foreground "dodgerblue4"))))
     (change-log-function ((t (:foreground "DodgerBlue4"))))
     (change-log-list ((t (:bold t :weight bold :foreground "slateblue4"))))
     (change-log-name ((t (:foreground "indianred4"))))
     (circe-highlight-nick-face ((t (:bold t :foreground "SteelBlue" :weight bold))))
     (circe-my-message-face ((t (:foreground "grey40"))))
     (circe-originator-face ((t (:foreground "cyan4"))))
     (circe-prompt-face ((t (:bold t :background "LightSeaGreen" :foreground "Black" :weight bold))))
     (circe-server-face ((t (:foreground "grey30"))))
     (circe-topic-diff-new-face ((t (:background "DarkGreen"))))
     (circe-topic-diff-removed-face ((t (:background "DarkRed"))))
     (compilation-column-number ((t (:foreground "darkslategrey"))))
     (compilation-error ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (compilation-info ((t (:bold t :weight bold))))
     (compilation-line-number ((t (:foreground "DodgerBlue4"))))
     (compilation-warning ((t (:bold t :foreground "Orange" :weight bold))))
     (completions-common-part ((t (:family "apple-monaco" :width normal :weight normal :slant normal :underline nil :overline nil :strike-through nil :box nil :inverse-video nil :foreground "black" :background "gainsboro" :stipple nil :height 120))))
     (completions-first-difference ((t (:bold t :weight bold))))
     (cursor ((t (:background "black"))))
     (custom-button ((t (:box (:line-width -1 :color "grey75") :foreground "black" :background "grey90"))))
     (custom-button-mouse ((t (:box (:line-width -1 :color "grey75") :background "white" :foreground "black"))))
     (custom-button-pressed ((t (:background "black" :foreground "white" :box (:line-width -1 :color "grey75")))))
     (custom-button-pressed-unraised ((t (:underline t :foreground "magenta4"))))
     (custom-button-unraised ((t (:underline t))))
     (custom-changed ((t (:background "blue1" :foreground "white"))))
     (custom-comment ((t (:background "gray85"))))
     (custom-comment-tag ((t (:foreground "blue4"))))
     (custom-documentation ((t (nil))))
     (custom-face-tag ((t (:bold t :weight bold))))
     (custom-group-tag ((t (:bold t :foreground "blue4" :weight bold))))
     (custom-group-tag-1 ((t (:bold t :family "helv" :foreground "red1" :weight bold :height 1.2))))
     (custom-invalid ((t (:background "red1" :foreground "yellow1"))))
     (custom-link ((t (:foreground "SteelBlue4"))))
     (custom-modified ((t (:background "blue1" :foreground "white"))))
     (custom-rogue ((t (:background "black" :foreground "pink"))))
     (custom-saved ((t (:underline t))))
     (custom-set ((t (:background "white" :foreground "blue1"))))
     (custom-state ((t (:foreground "dark green"))))
     (custom-themed ((t (:background "blue1" :foreground "white"))))
     (custom-variable-button ((t (:bold t :underline t :weight bold))))
     (custom-variable-tag ((t (:bold t :foreground "blue3" :weight bold))))
     (darcsum-change-line-face ((t (:background "grey75" :foreground "grey25"))))
     (darcsum-filename-face ((t (:foreground "blue4"))))
     (darcsum-header-face ((t (:bold t :foreground "navy" :weight bold))))
     (darcsum-marked-face ((t (:bold t :weight bold))))
     (darcsum-need-action-face ((t (:foreground "indianred4"))))
     (darcsum-need-action-marked-face ((t (:bold t :foreground "orange" :weight bold))))
     (diff-added ((t (nil))))
     (diff-changed ((t (nil))))
     (diff-context ((t (:foreground "grey50"))))
     (diff-file-header ((t (:bold t :background "grey70" :weight bold))))
     (diff-function ((t (:background "grey85"))))
     (diff-header ((t (:background "grey85"))))
     (diff-hunk-header ((t (:background "grey85"))))
     (diff-index ((t (:bold t :weight bold :background "grey70"))))
     (diff-indicator-added ((t (nil))))
     (diff-indicator-changed ((t (nil))))
     (diff-indicator-removed ((t (nil))))
     (diff-nonexistent ((t (:bold t :weight bold :background "grey70"))))
     (diff-removed ((t (nil))))
     (dired-directory ((t (:foreground "dodgerblue4"))))
     (dired-flagged ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (dired-header ((t (:foreground "darkslategrey"))))
     (dired-ignored ((t (:foreground "grey50"))))
     (dired-mark ((t (:foreground "indianred4"))))
     (dired-marked ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (dired-symlink ((t (:bold t :weight bold :foreground "slateblue4"))))
     (dired-warn-writable ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (dired-warning ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (epa-field-body ((t (:italic t :slant italic))))
     (epa-field-name ((t (:bold t :weight bold))))
     (epa-mark ((t (:bold t :foreground "red" :weight bold))))
     (epa-string ((t (:foreground "blue4"))))
     (epa-validity-disabled ((t (:italic t :slant italic))))
     (epa-validity-high ((t (:bold t :weight bold))))
     (epa-validity-low ((t (:italic t :slant italic))))
     (epa-validity-medium ((t (:italic t :slant italic))))
     (escape-glyph ((t (:foreground "brown"))))
     (eshell-ls-archive ((t (:bold t :foreground "Orchid4" :weight bold))))
     (eshell-ls-backup ((t (:foreground "OrangeRed"))))
     (eshell-ls-clutter ((t (:bold t :foreground "OrangeRed" :weight bold))))
     (eshell-ls-directory ((t (:bold t :foreground "darkblue" :weight bold))))
     (eshell-ls-executable ((t (:bold t :foreground "ForestGreen" :weight bold))))
     (eshell-ls-missing ((t (:bold t :foreground "Red" :weight bold))))
     (eshell-ls-product ((t (:foreground "OrangeRed"))))
     (eshell-ls-readonly ((t (:foreground "Brown"))))
     (eshell-ls-special ((t (:bold t :foreground "Magenta" :weight bold))))
     (eshell-ls-symlink ((t (:bold t :foreground "Dark Cyan" :weight bold))))
     (eshell-ls-unreadable ((t (:foreground "Grey30"))))
     (eshell-prompt ((t (:bold t :foreground "cyan4" :weight bold))))
     (ffap ((t (:background "white"))))
     (file-name-shadow ((t (:foreground "grey50"))))
     (fixed-pitch ((t (:family "courier"))))
     (flyspell-duplicate ((t (:bold t :foreground "Gold3" :underline t :weight bold))))
     (flyspell-incorrect ((t (:bold t :foreground "OrangeRed" :underline t :weight bold))))
     (font-lock-builtin-face ((t (:foreground "darkslategrey"))))
     (font-lock-comment-delimiter-face ((t (:foreground "grey40"))))
     (font-lock-comment-face ((t (:foreground "grey40"))))
     (font-lock-constant-face ((t (:foreground "indianred4"))))
     (font-lock-doc-face ((t (:foreground "dimgray"))))
     (font-lock-function-name-face ((t (:foreground "dodgerblue4"))))
     (font-lock-keyword-face ((t (:bold t :foreground "slateblue4" :weight bold))))
     (font-lock-negation-char-face ((t (nil))))
     (font-lock-preprocessor-face ((t (:foreground "darkslategrey"))))
     (font-lock-regexp-grouping-backslash ((t (:bold t :weight bold))))
     (font-lock-regexp-grouping-construct ((t (:bold t :weight bold))))
     (font-lock-string-face ((t (:foreground "darkseagreen4"))))
     (font-lock-type-face ((t (:foreground "darkslategrey"))))
     (font-lock-variable-name-face ((t (:foreground "DodgerBlue4"))))
     (font-lock-warning-face ((t (:bold t :background "black" :foreground "red" :weight bold))))
     (fringe ((t (nil))))
     (gnus-cite-1 ((t (:foreground "MidnightBlue"))))
     (gnus-cite-10 ((t (:foreground "medium purple"))))
     (gnus-cite-11 ((t (:foreground "turquoise"))))
     (gnus-cite-2 ((t (:foreground "slateblue"))))
     (gnus-cite-3 ((t (:foreground "dark green"))))
     (gnus-cite-4 ((t (:foreground "grey34"))))
     (gnus-cite-5 ((t (:foreground "grey43"))))
     (gnus-cite-6 ((t (:foreground "dark violet"))))
     (gnus-cite-7 ((t (:foreground "SteelBlue4"))))
     (gnus-cite-8 ((t (:foreground "magenta"))))
     (gnus-cite-9 ((t (:foreground "violet"))))
     (gnus-cite-attribution ((t (:italic t :slant italic))))
     (gnus-emphasis-bold ((t (:bold t :weight bold))))
     (gnus-emphasis-bold-italic ((t (:italic t :bold t :slant italic :weight bold))))
     (gnus-emphasis-highlight-words ((t (:background "black" :foreground "yellow"))))
     (gnus-emphasis-italic ((t (:italic t :slant italic))))
     (gnus-emphasis-strikethru ((t (:strike-through t))))
     (gnus-emphasis-underline ((t (:underline t))))
     (gnus-emphasis-underline-bold ((t (:bold t :underline t :weight bold))))
     (gnus-emphasis-underline-bold-italic ((t (:italic t :bold t :underline t :slant italic :weight bold))))
     (gnus-emphasis-underline-italic ((t (:italic t :underline t :slant italic))))
     (gnus-group-mail-1 ((t (:bold t :foreground "DeepPink3" :weight bold))))
     (gnus-group-mail-1-empty ((t (:foreground "DeepPink3"))))
     (gnus-group-mail-2 ((t (:bold t :foreground "HotPink3" :weight bold))))
     (gnus-group-mail-2-empty ((t (:foreground "HotPink3"))))
     (gnus-group-mail-3 ((t (:bold t :foreground "slateblue" :weight bold))))
     (gnus-group-mail-3-empty ((t (:foreground "grey55"))))
     (gnus-group-mail-low ((t (:bold t :foreground "DeepPink4" :weight bold))))
     (gnus-group-mail-low-empty ((t (:foreground "DeepPink4"))))
     (gnus-group-news-1 ((t (:bold t :foreground "ForestGreen" :weight bold))))
     (gnus-group-news-1-empty ((t (:foreground "ForestGreen"))))
     (gnus-group-news-2 ((t (:bold t :foreground "dodgerblue4" :weight bold))))
     (gnus-group-news-2-empty ((t (:foreground "CadetBlue4"))))
     (gnus-group-news-3 ((t (:bold t :weight bold))))
     (gnus-group-news-3-empty ((t (nil))))
     (gnus-group-news-4 ((t (:bold t :weight bold))))
     (gnus-group-news-4-empty ((t (nil))))
     (gnus-group-news-5 ((t (:bold t :weight bold))))
     (gnus-group-news-5-empty ((t (nil))))
     (gnus-group-news-6 ((t (:bold t :weight bold))))
     (gnus-group-news-6-empty ((t (nil))))
     (gnus-group-news-low ((t (:bold t :foreground "DarkGreen" :weight bold))))
     (gnus-group-news-low-empty ((t (:foreground "DarkGreen"))))
     (gnus-header-content ((t (:italic t :foreground "slategrey" :slant italic))))
     (gnus-header-from ((t (:foreground "slateblue"))))
     (gnus-header-name ((t (:bold t :foreground "slategrey" :weight bold))))
     (gnus-header-newsgroups ((t (:italic t :foreground "MidnightBlue" :slant italic))))
     (gnus-header-subject ((t (:foreground "darkslateblue" :bold t))))
     (gnus-server-agent ((t (:bold t :weight bold))))
     (gnus-server-closed ((t (:italic t :foreground "Steel Blue" :slant italic))))
     (gnus-server-denied ((t (:bold t :foreground "Red" :weight bold))))
     (gnus-server-offline ((t (:bold t :foreground "Orange" :weight bold))))
     (gnus-server-opened ((t (:bold t :foreground "Green3" :weight bold))))
     (gnus-signature ((t (:italic t :slant italic))))
     (gnus-splash ((t (:foreground "#888888"))))
     (gnus-summary-cancelled ((t (:background "black" :foreground "yellow"))))
     (gnus-summary-high-ancient ((t (:bold t :foreground "RoyalBlue" :weight bold))))
     (gnus-summary-high-read ((t (:bold t :foreground "DarkGreen" :weight bold))))
     (gnus-summary-high-ticked ((t (:bold t :foreground "firebrick" :weight bold))))
     (gnus-summary-high-undownloaded ((t (:bold t :foreground "cyan4" :weight bold))))
     (gnus-summary-high-unread ((t (:bold t :weight bold))))
     (gnus-summary-low-ancient ((t (:italic t :foreground "RoyalBlue" :slant italic))))
     (gnus-summary-low-read ((t (:italic t :foreground "DarkGreen" :slant italic))))
     (gnus-summary-low-ticked ((t (:italic t :foreground "firebrick" :slant italic))))
     (gnus-summary-low-undownloaded ((t (:italic t :foreground "cyan4" :slant italic :weight normal))))
     (gnus-summary-low-unread ((t (:italic t :slant italic))))
     (gnus-summary-normal-ancient ((t (:foreground "RoyalBlue"))))
     (gnus-summary-normal-read ((t (:foreground "DarkGreen"))))
     (gnus-summary-normal-ticked ((t (:foreground "firebrick"))))
     (gnus-summary-normal-undownloaded ((t (:foreground "cyan4" :weight normal))))
     (gnus-summary-normal-unread ((t (nil))))
     (gnus-summary-selected ((t (:underline t))))
     (gnus-x-face ((t (:background "white" :foreground "black"))))
     (header-line ((t (:box (:line-width -1 :color "grey75") :background "grey90" :foreground "grey20" :box nil))))
     (help-argument-name ((t (:italic t :slant italic))))
     (highlight ((t (:background "white"))))
     (info-header-node ((t (:italic t :bold t :weight bold :slant italic :foreground "brown"))))
     (info-header-xref ((t (:foreground "SteelBlue4"))))
     (info-menu-header ((t (:bold t :family "helv" :weight bold))))
     (info-menu-star ((t (:foreground "red1"))))
     (info-node ((t (:italic t :bold t :foreground "brown" :slant italic :weight bold))))
     (info-title-1 ((t (:bold t :weight bold :family "helv" :height 1.728))))
     (info-title-2 ((t (:bold t :family "helv" :weight bold :height 1.44))))
     (info-title-3 ((t (:bold t :weight bold :family "helv" :height 1.2))))
     (info-title-4 ((t (:bold t :family "helv" :weight bold))))
     (info-xref ((t (:foreground "SteelBlue4"))))
     (info-xref-visited ((t (:foreground "navy"))))
     (isearch ((t (:background "magenta3" :foreground "lightskyblue1"))))
     (iswitchb-current-match ((t (:foreground "dodgerblue4"))))
     (iswitchb-invalid-regexp ((t (:bold t :weight bold :foreground "red" :background "black"))))
     (iswitchb-single-match ((t (:foreground "grey40"))))
     (iswitchb-virtual-matches ((t (:foreground "darkslategrey"))))
     (italic ((t (:italic t :slant italic))))
     (lazy-highlight ((t (:background "paleturquoise"))))
     (link ((t (:foreground "SteelBlue4"))))
     (link-visited ((t (:foreground "navy"))))
     (lui-button-face ((t (:foreground "Purple" :underline t))))
     (lui-highlight-face ((t (:foreground "Purple"))))
     (lui-irc-colors-bg-0-face ((t (:background "white"))))
     (lui-irc-colors-bg-1-face ((t (:background "black"))))
     (lui-irc-colors-bg-10-face ((t (:background "cyan4"))))
     (lui-irc-colors-bg-11-face ((t (:background "cyan"))))
     (lui-irc-colors-bg-12-face ((t (:background "blue"))))
     (lui-irc-colors-bg-13-face ((t (:background "magenta"))))
     (lui-irc-colors-bg-14-face ((t (:background "dimgray"))))
     (lui-irc-colors-bg-15-face ((t (:background "gray"))))
     (lui-irc-colors-bg-2-face ((t (:background "blue4"))))
     (lui-irc-colors-bg-3-face ((t (:background "green4"))))
     (lui-irc-colors-bg-4-face ((t (:background "red"))))
     (lui-irc-colors-bg-5-face ((t (:background "red4"))))
     (lui-irc-colors-bg-6-face ((t (:background "magenta4"))))
     (lui-irc-colors-bg-7-face ((t (:background "yellow4"))))
     (lui-irc-colors-bg-8-face ((t (:background "yellow"))))
     (lui-irc-colors-bg-9-face ((t (:background "green"))))
     (lui-irc-colors-fg-0-face ((t (:foreground "white"))))
     (lui-irc-colors-fg-1-face ((t (:foreground "black"))))
     (lui-irc-colors-fg-10-face ((t (:foreground "cyan4"))))
     (lui-irc-colors-fg-11-face ((t (:foreground "cyan"))))
     (lui-irc-colors-fg-12-face ((t (:foreground "blue"))))
     (lui-irc-colors-fg-13-face ((t (:foreground "magenta"))))
     (lui-irc-colors-fg-14-face ((t (:foreground "dimgray"))))
     (lui-irc-colors-fg-15-face ((t (:foreground "gray"))))
     (lui-irc-colors-fg-2-face ((t (:foreground "blue4"))))
     (lui-irc-colors-fg-3-face ((t (:foreground "green4"))))
     (lui-irc-colors-fg-4-face ((t (:foreground "red"))))
     (lui-irc-colors-fg-5-face ((t (:foreground "red4"))))
     (lui-irc-colors-fg-6-face ((t (:foreground "magenta4"))))
     (lui-irc-colors-fg-7-face ((t (:foreground "yellow4"))))
     (lui-irc-colors-fg-8-face ((t (:foreground "yellow"))))
     (lui-irc-colors-fg-9-face ((t (:foreground "green"))))
     (lui-irc-colors-inverse-face ((t (nil))))
     (lui-time-stamp-face ((t (:foreground "LightSlateGrey"))))
     (ma-objc-dox-highlight-face ((t (:background "grey70"))))
     (mac-ts-block-fill-text ((t (:underline t))))
     (mac-ts-caret-position ((t (nil))))
     (mac-ts-converted-text ((t (:underline "gray80"))))
     (mac-ts-no-hilite ((t (:family "apple-monaco" :width normal :weight normal :slant normal :underline nil :overline nil :strike-through nil :box nil :inverse-video nil :foreground "black" :background "gainsboro" :stipple nil :height 120))))
     (mac-ts-outline-text ((t (:underline t))))
     (mac-ts-raw-text ((t (:underline t))))
     (mac-ts-selected-converted-text ((t (:underline t))))
     (mac-ts-selected-raw-text ((t (:underline t))))
     (mac-ts-selected-text ((t (:underline t))))
     (match ((t (:background "lightblue"))))
     (menu ((t (nil))))
     (message-cited-text ((t (:foreground "firebrick4"))))
     (message-header-cc ((t (:foreground "MidnightBlue"))))
     (message-header-name ((t (:foreground "cornflower blue"))))
     (message-header-newsgroups ((t (:italic t :bold t :foreground "blue4" :slant italic :weight bold))))
     (message-header-other ((t (:foreground "steel blue"))))
     (message-header-subject ((t (:bold t :foreground "navy blue" :weight bold))))
     (message-header-to ((t (:bold t :foreground "MidnightBlue" :weight bold))))
     (message-header-xheader ((t (:foreground "blue"))))
     (message-mml ((t (:foreground "ForestGreen"))))
     (message-separator ((t (:foreground "brown"))))
     (minibuffer-prompt ((t (:foreground "medium blue"))))
     (mode-line ((t (:background "grey90" :foreground "black" :box (:line-width -1 :color "grey75")))))
     (mode-line-buffer-id ((t (nil))))
     (mode-line-highlight ((t (:box (:line-width -1 :color "grey75") :foreground "black" :background "white"))))
     (mode-line-inactive ((t (:background "grey83" :foreground "grey30" :box (:line-width -1 :color "grey60") :weight light))))
     (mouse ((t (:background "black"))))
     (muse-bad-link ((t (:bold t :foreground "red" :underline "red" :weight bold))))
     (muse-emphasis-1 ((t (:italic t :slant italic))))
     (muse-emphasis-2 ((t (:bold t :weight bold))))
     (muse-emphasis-3 ((t (:italic t :bold t :slant italic :weight bold))))
     (muse-header-1 ((t (:foreground "dodgerblue4"))))
     (muse-header-2 ((t (:foreground "DodgerBlue4"))))
     (muse-header-3 ((t (:bold t :weight bold :foreground "slateblue4"))))
     (muse-header-4 ((t (:foreground "darkslategrey"))))
     (muse-header-5 ((t (:foreground "grey40"))))
     (muse-link ((t (:foreground "SteelBlue4"))))
     (muse-verbatim ((t (:foreground "dark slate gray" :height 140 :family "Courier"))))
     (next-error ((t (:background "lightgoldenrod2"))))
     (nobreak-space ((t (:foreground "brown" :underline t))))
     (org-agenda-restriction-lock ((t (:background "yellow1"))))
     (org-agenda-structure ((t (:foreground "DodgerBlue4"))))
     (org-archived ((t (:foreground "grey50"))))
     (org-code ((t (:foreground "grey50"))))
     (org-column ((t (:background "grey90" :height 120 :family "apple-monaco"))))
     (org-date ((t (:foreground "cyan4"))))
     (org-done ((t (:bold t :foreground "DarkSeaGreen4" :weight bold))))
     (org-drawer ((t (:foreground "Blue1"))))
     (org-ellipsis ((t (:foreground "DarkGoldenrod"))))
     (org-formula ((t (:foreground "Firebrick"))))
     (org-headline-done ((t (:foreground "RosyBrown"))))
     (org-hide ((t (:foreground "white"))))
     (org-latex-and-export-specials ((t (:foreground "SaddleBrown"))))
     (org-level-1 ((t (:bold t :foreground "DarkSlateGrey" :weight bold))))
     (org-level-2 ((t (:bold t :foreground "slategrey" :weight bold))))
     (org-level-3 ((t (:foreground "DarkSeaGreen4"))))
     (org-level-4 ((t (:foreground "Firebrick"))))
     (org-level-5 ((t (:foreground "ForestGreen"))))
     (org-level-6 ((t (:foreground "CadetBlue"))))
     (org-level-7 ((t (:foreground "Orchid"))))
     (org-level-8 ((t (:foreground "RosyBrown"))))
     (org-link ((t (:foreground "SteelBlue4" :underline t))))
     (org-property-value ((t (nil))))
     (org-scheduled-previously ((t (:foreground "Firebrick"))))
     (org-scheduled-today ((t (:foreground "DarkGreen"))))
     (org-sexp-date ((t (:foreground "Purple"))))
     (org-special-keyword ((t (:foreground "RosyBrown"))))
     (org-table ((t (:foreground "Navy"))))
     (org-tag ((t (:bold t :weight bold))))
     (org-target ((t (:underline t))))
     (org-time-grid ((t (:foreground "DarkGoldenrod"))))
     (org-todo ((t (:bold t :foreground "Red4" :weight bold))))
     (org-upcoming-deadline ((t (:foreground "Firebrick"))))
     (org-verbatim ((t (:foreground "grey50" :underline t))))
     (org-warning ((t (:bold t :foreground "Red1" :weight bold))))
     (outline-1 ((t (:foreground "dodgerblue4"))))
     (outline-2 ((t (:foreground "DodgerBlue4"))))
     (outline-3 ((t (:bold t :weight bold :foreground "slateblue4"))))
     (outline-4 ((t (:foreground "darkslategrey"))))
     (outline-5 ((t (:foreground "grey40"))))
     (outline-6 ((t (:foreground "indianred4"))))
     (outline-7 ((t (:foreground "darkslategrey"))))
     (outline-8 ((t (:foreground "darkseagreen4"))))
     (query-replace ((t (:foreground "lightskyblue1" :background "magenta3"))))
     (region ((t (:background "lightgoldenrod2"))))
     (rs-gnus-face-1 ((t (:stipple nil :background "gainsboro" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :family "apple-monaco"))))
     (scroll-bar ((t (nil))))
     (secondary-selection ((t (:background "yellow1"))))
     (shadow ((t (:foreground "grey50"))))
     (show-paren-match ((t (:background "turquoise"))))
     (show-paren-mismatch ((t (:background "purple" :foreground "white"))))
     (speedbar-button-face ((t (:foreground "green4"))))
     (speedbar-directory-face ((t (:foreground "blue4"))))
     (speedbar-file-face ((t (:foreground "cyan4"))))
     (speedbar-highlight-face ((t (:background "green"))))
     (speedbar-selected-face ((t (:foreground "red" :underline t))))
     (speedbar-separator-face ((t (:background "blue" :foreground "white" :overline "gray"))))
     (speedbar-tag-face ((t (:foreground "brown"))))
     (tool-bar ((t (:background "grey75" :foreground "black" :box (:line-width 1 :style released-button)))))
     (tooltip ((t (:family "helv" :background "lightyellow" :foreground "black"))))
     (trailing-whitespace ((t (:background "red1"))))
     (underline ((t (:underline t))))
     (variable-pitch ((t (:family "helv"))))
     (vertical-border ((t (:background "white" :foreground "black"))))
     (w3m-anchor ((t (:foreground "SteelBlue4"))))
     (w3m-arrived-anchor ((t (:foreground "navy"))))
     (w3m-bold ((t (:bold t :weight bold))))
     (w3m-current-anchor ((t (:bold t :underline t :weight bold))))
     (w3m-form ((t (:foreground "cyan4" :underline t))))
     (w3m-form-button ((t (:background "grey90" :foreground "black" :box (:line-width -1 :color "grey75")))))
     (w3m-form-button-mouse ((t (:background "grey90" :foreground "black" :box (:line-width -1 :color "grey75")))))
     (w3m-form-button-pressed ((t (:background "lightgrey" :foreground "black" :box (:line-width 2 :style pressed-button)))))
     (w3m-form-face ((t (:foreground "cyan4" :underline t))))
     (w3m-header-line-location-content ((t (:italic t :box (:line-width 3 :color "gainsboro") :slant italic))))
     (w3m-header-line-location-title ((t (:italic t :slant italic :box (:line-width 3 :color "gainsboro")))))
     (w3m-history-current-url ((t (:background "yellow1" :foreground "navy"))))
     (w3m-image ((t (:foreground "ForestGreen"))))
     (w3m-insert ((t (:foreground "purple"))))
     (w3m-italic ((t (:italic t :slant italic))))
     (w3m-session-select ((t (:foreground "dark blue"))))
     (w3m-session-selected ((t (:bold t :foreground "dark blue" :underline t :weight bold))))
     (w3m-strike-through ((t (:strike-through t))))
     (w3m-tab-background ((t (:foreground "black"))))
     (w3m-tab-mouse ((t (:background "Gray75" :foreground "white" :box (:line-width -1 :style released-button)))))
     (w3m-tab-selected ((t (:box (:line-width -1 :color "grey60") :weight light :background "lightblue" :foreground "black"))))
     (w3m-tab-selected-background ((t (:background "LightSteelBlue" :foreground "black"))))
     (w3m-tab-selected-retrieving ((t (:background "lightblue" :weight light :box (:line-width -1 :color "grey60") :foreground "red"))))
     (w3m-tab-unselected ((t (:weight light :box (:line-width -1 :color "grey60") :foreground "grey30" :background "grey83"))))
     (w3m-tab-unselected-retrieving ((t (:background "grey83" :box (:line-width -1 :color "grey60") :weight light :foreground "OrangeRed"))))
     (w3m-underline ((t (:underline t))))
     (widget-button ((t (:bold t :weight bold))))
     (widget-button-pressed ((t (:foreground "red1"))))
     (widget-documentation ((t (:foreground "dark green"))))
     (widget-field ((t (:background "gray85"))))
     (widget-inactive ((t (:foreground "grey50"))))
     (widget-single-line-field ((t (:background "gray85")))))))
(add-to-list 'color-themes '(color-theme-jao-osx  "jao-osx" "jao"))