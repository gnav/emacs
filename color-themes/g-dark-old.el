(eval-when-compile    (require 'color-theme))
(defun color-theme-g-dark ()
  "Color theme based on blueish (jao). Guillermo Navarro"
  (interactive)
  (color-theme-install
   '(color-theme-g-dark
     ((mouse-color . "white")
      (background-color . "grey2")
      (background-mode . dark)
      (border-color . "Grey")
      (cursor-color . "Grey")
      (foreground-color . "#bbb"))
     (default ((t (:family "fixed"))))
     (mouse ((t (:background "white"))))
     (Info-title-1-face
      ((t (:bold t :weight bold :family "terminus" :height 1.728))))
     (Info-title-2-face
      ((t (:bold t :family "terminus" :weight bold :height 1.44))))
     (Info-title-3-face
      ((t (:bold t :weight bold :family "terminus" :height 1.2))))
     (Info-title-4-face ((t (:bold t :family "terminus" :weight bold))))
     (bbdb-company ((t (:italic t :slant italic))))
     (bbdb-field-name ((t (:bold t :weight bold))))
     (bbdb-field-value ((t (nil))))
     (bbdb-name ((t (:underline t :family "fixed"))))
     (bmk-mgr-bookmark-face ((t (nil))))
     (bmk-mgr-sel-bookmark-face ((t (:foreground "slate blue"))))
     (bmk-mgr-folder-face ((t (:bold t :weight bold :family "modd"))))
     (bmk-mgr-sel-folder-face
      ((t (:bold t :foreground "slate blue" :weight bold :family "modd"))))
     (bold ((t (:bold t :weight bold))))
     (bold-italic ((t (:bold t :foreground "beige" :weight bold))))
     (border ((t (:background "white"))))
     (calendar-today-face ((t (:underline t :family "modd"))))
     (comint-highlight-input ((t (:italic t :slant italic))))
     (comint-highlight-prompt ((t (:bold t :weight bold :foreground "white"))))
     (cscope-file-face ((t (:foreground "dark slate blue"))))
     (cscope-function-face ((t (:foreground "dark slate grey"))))
     (cscope-line-face((t (:foreground "#666"))))
     (cscope-line-number-face ((t (:foreground "slate grey"))))
     (cscope-mouse-face((t (:foreground "#666" :background "grey2"))))
     (cursor ((t (:background "Grey"))))
     (custom-button-face ((t (:foreground "slate grey"))))
     (custom-button-pressed-face
      ((t (:background "lightgrey" :foreground "black"
                       :box (:line-width 2 :style pressed-button)))))
     (custom-changed-face ((t (:background "blue" :foreground "white"))))
     (custom-comment-face
      ((t (:italic t :slant italic :background "gainsboro"))))
     (custom-comment-tag-face ((t (:foreground "gainsboro"))))
     (custom-documentation-face ((t (:foreground "dark slate grey"))))
     (custom-face-tag-face ((t (:underline t :family "fixed"))))
     (custom-group-tag-face ((t (:foreground "grey"))))
     (custom-group-tag-face-1
      ((t (:foreground "pale turquoise" :underline t :family "fixed"))))
     (custom-invalid-face ((t (:foreground "red"))))
     (custom-modified-face ((t (:background "blue" :foreground "white"))))
     (custom-rogue-face ((t (:background "black" :foreground "gainsboro"))))
     (custom-saved-face ((t (:underline t :family "fixed"))))
     (custom-set-face ((t (:background "grey" :foreground "blue"))))
     (custom-state-face ((t (:foreground "#444"))))
     (custom-variable-button-face ((t (:bold t :underline t :weight bold))))
     (custom-variable-tag-face ((t (:foreground "slate blue"))))
     (cvs-filename-face ((t (:foreground "dark slate grey"))))
     (cvs-header-face ((t (:foreground "white"))))
     (cvs-marked-face ((t (:foreground "light salmon"))))
     (cvs-need-action-face ((t (:foreground "dark slate blue"))))
     (cvs-unknown-face ((t (:foreground "light salmon"))))
     (diary-face ((t (:foreground "red"))))
     (diff-added-face ((t (:foreground "slate blue"))))
     (diff-changed-face ((t (:foreground "slate blue"))))
     (diff-context-face ((t (nil))))
     (diff-file-header-face ((t (:bold t :weight bold))))
     (diff-function-face ((t (nil))))
     (diff-header-face ((t (:family "modd"))))
     (diff-hunk-header-face ((t (:foreground "dark slate blue"))))
     (diff-index-face ((t (:bold t :weight bold))))
     (diff-nonexistent-face ((t (:bold t :background "grey70" :weight bold))))
     (diff-removed-face ((t (:foreground "dark slate grey"))))
     (dired-face-directory ((t (:bold t :foreground "sky blue" :weight bold))))
     (dired-face-executable ((t (:foreground "green yellow"))))
     (dired-face-flagged ((t (:foreground "tomato"))))
     (dired-face-marked ((t (:foreground "light salmon"))))
     (dired-face-permissions ((t (:foreground "aquamarine"))))
     (ecb-bucket-token-face ((t (:bold t :weight bold))))
     (ecb-default-general-face ((t (:height 1.0))))
     (ecb-default-highlight-face
      ((t (:underline t :family "fixed" :height 1.0))))
     (ecb-directories-general-face ((t (:height 1.0))))
     (ecb-directory-face ((t (:background "grey50" :height 1.0))))
     (ecb-history-face ((t (:background "grey50" :height 1.0))))
     (ecb-history-general-face ((t (:height 1.0))))
     (ecb-method-face ((t (:background "grey50" :height 1.0))))
     (ecb-methods-general-face ((t (:height 1.0))))
     (ecb-source-face ((t (:background "grey50" :height 1.0))))
     (ecb-source-in-directories-buffer-face
      ((t (:foreground "dark slate grey" :height 1.0))))
     (ecb-sources-general-face ((t (:height 1.0))))
     (ecb-token-header-face
      ((t (:foreground "black" :background "dark slate grey"))))
     (ecb-type-token-class-face ((t (:bold t :weight bold))))
     (ecb-type-token-enum-face ((t (:bold t :weight bold))))
     (ecb-type-token-group-face
      ((t (:bold t :foreground "dim gray" :weight bold))))
     (ecb-type-token-interface-face ((t (:bold t :weight bold))))
     (ecb-type-token-struct-face ((t (:bold t :weight bold))))
     (ecb-type-token-typedef-face ((t (:bold t :weight bold))))
     (ediff-current-diff-face-A
      ((t (:background "pale green" :foreground "firebrick"))))
     (ediff-current-diff-face-Ancestor
      ((t (:background "VioletRed" :foreground "Black"))))
     (ediff-current-diff-face-B
      ((t (:background "Yellow" :foreground "DarkOrchid"))))
     (ediff-current-diff-face-C ((t (:background "Pink" :foreground "Navy"))))
     (ediff-even-diff-face-A
      ((t (:background "light grey" :foreground "Black"))))
     (ediff-even-diff-face-Ancestor
      ((t (:background "Grey" :foreground "White"))))
     (ediff-even-diff-face-B ((t (:background "Grey" :foreground "White"))))
     (ediff-even-diff-face-C
      ((t (:background "light grey" :foreground "Black"))))
     (ediff-fine-diff-face-A ((t (:background "sky blue" :foreground "Navy"))))
     (ediff-fine-diff-face-Ancestor
      ((t (:background "Green" :foreground "Black"))))
     (ediff-fine-diff-face-B ((t (:background "cyan" :foreground "Black"))))
     (ediff-fine-diff-face-C
      ((t (:background "Turquoise" :foreground "Black"))))
     (ediff-odd-diff-face-A ((t (:background "Grey" :foreground "White"))))
     (ediff-odd-diff-face-Ancestor
      ((t (:background "light grey" :foreground "Black"))))
     (ediff-odd-diff-face-B
      ((t (:background "light grey" :foreground "Black"))))
     (ediff-odd-diff-face-C ((t (:background "Grey" :foreground "White"))))
     (emacs-wiki-bad-link-face ((t (:foreground "light salmon" :weight bold))))
     (emacs-wiki-header-1
      ((t (:bold t :weight bold :height 1.2 :family "terminus"))))
     (emacs-wiki-header-2
      ((t (:bold t :weight bold :height 1.2 :family "terminus"))))
     (emacs-wiki-header-3
      ((t (:bold t :foreground "dark slate grey"
                 :weight bold :height 120 :family "Helvetica"))))
     (emacs-wiki-header-4 ((t (:height 120 :family "modd"))))
     (emacs-wiki-header-5 ((t (:height 120 :family "modd"))))
     (emacs-wiki-header-6 ((t (:height 120 :family "modd"))))
     (emacs-wiki-link-face ((t (:foreground "dark slate grey" :family "modd"))))
     (erc-action-face ((t (:foreground "dark slate grey"))))
     (erc-bold-face ((t (nil))))
     (erc-current-nick-face ((t (:foreground "dark slate blue"))))
     (erc-default-face ((t (:foreground "Grey40"))))
     (erc-direct-msg-face ((t (:family "modd"))))
     (erc-error-face ((t (:foreground "slategrey"))))
     (erc-highlight-face ((t (:foreground "lightsalmon"))))
     (erc-input-face ((t (:foreground "dark slate blue"))))
     (erc-inverse-face ((t (:background "grey"))))
     (erc-nick-default-face
      ((t (:foreground "dark slate grey" :weight bold :bold t))))
     (erc-nick-msg-face
      ((t (:foreground "dim grey" :weight bold :bold t))))
     (erc-notice-face ((t (:foreground "#444"))))
     (erc-pal-face ((t (:foreground "dark slate grey"))))
     (erc-prompt-face ((t (:foreground "dark slate blue"))))
     (erc-timestamp-face ((t (:foreground "dark slate grey"))))
     (eshell-ls-archive-face ((t (:foreground "medium purple"))))
     (eshell-ls-backup-face ((t (:foreground "dim gray"))))
     (eshell-ls-clutter-face ((t (:foreground "dim gray"))))
     (eshell-ls-directory-face ((t (:foreground "slate blue"))))
     (eshell-ls-executable-face ((t (:foreground "slate grey"))))
     (eshell-ls-missing-face ((t (:foreground "gold"))))
     (eshell-ls-picture-face ((t (nil))))
     (eshell-ls-product-face ((t (:foreground "light steel blue"))))
     (eshell-ls-readonly-face ((t (:foreground "indianred"))))
     (eshell-ls-special-face ((t (:foreground "gold"))))
     (eshell-ls-symlink-face ((t (:foreground "grey30" :family "modd"))))
     (eshell-ls-unreadable-face ((t (:foreground "dim gray"))))
     (eshell-prompt-face ((t (:foreground "dark slate grey" :family "modd"))))
     (eshell-tab-selected-face
      ((t (:background "grey10" :foreground "grey60"
                       :box (:line-width 1 :style nil) :family "modd"))))
     (eshell-tab-unselected-face
      ((t (:background "grey10" :foreground "grey40"
                       :box (:line-width 1 :style nil) :family "modd"))))
     (excerpt ((t (:italic t :slant italic))))
     (fixed ((t (:bold t :weight bold))))
     (fixed-pitch ((t (:family "courier"))))
     (flyspell-duplicate-face
      ((t (:bold t :foreground "Gold3" :underline t :weight bold))))
     (flyspell-incorrect-face
      ((t (:bold t :foreground "firebrick3" :underline t :weight bold))))
     (font-latex-bold-face
      ((t (:bold t :foreground "SlateBlue1" :weight bold))))
     (font-latex-italic-face
      ((t (:foreground "SlateBlue1" :family "modd"))))
;;      (font-latex-italic-face
;;       ((t (:foreground "#bbb" :slant italic))))
     (font-latex-math-face ((t (:foreground "CadetBlue4"))))
     (font-latex-sedate-face ((t (:foreground "LightGray"))))
     (font-latex-string-face ((t (:foreground "slate grey"))))
     (font-latex-title-1-face ((t (:foreground "slate grey" :heigh 1.5))))
     (font-latex-title-2-face ((t (:foreground "slate grey" :heigh 1.5))))
     (font-latex-title-3-face ((t (:foreground "slate grey"))))
     (font-latex-title-4-face ((t (:foreground "slate grey"))))
     (font-latex-sectioning-1-face ((t (:foreground "slate grey"))))
     (font-latex-sectioning-2-face ((t (:foreground "slate grey"))))
     (font-latex-sectioning-3-face ((t (:foreground "slate grey"))))
     (font-latex-sectioning-4-face ((t (:foreground "slate grey"))))
     (font-latex-warning-face ((t (:foreground "slate grey"))))
     (font-lock-builtin-face ((t (:foreground "slate blue"))))
     (font-lock-comment-face ((t (:foreground "#777" :family "modd"))))
     (font-lock-constant-face ((t (:foreground "cyan4"))))
     (font-lock-doc-face ((t (:foreground "gray60"))))
     (font-lock-doc-string-face ((t (:foreground "gray60"))))
     (font-lock-function-name-face ((t (:foreground "CadetBlue4"))))
     (font-lock-keyword-face ((t (:foreground "SlateBlue3"))))
     (font-lock-reference-face ((t (:foreground "dark khaki"))))
     (font-lock-string-face ((t (:foreground "slate grey"))))
     (font-lock-type-face ((t (:foreground "SlateBlue1"))))
     (font-lock-variable-name-face ((t (:foreground "DarkSlateGray3"))))
     (font-lock-warning-face ((t (:bold t :foreground "slate grey"))))
     (fringe ((t (:background "grey5"))))
     (gnus-cite-attribution-face ((t (:italic t :slant italic))))
     (gnus-cite-face-1 ((t (:foreground "dark slate grey"))))
     (gnus-cite-face-2 ((t (:foreground "dark slate blue"))))
     (gnus-cite-face-3 ((t (:foreground "dark slate grey"))))
     (gnus-cite-face-4 ((t (:foreground "dark slate blue"))))
     (gnus-cite-face-5 ((t (:foreground "dark slate grey"))))
     (gnus-cite-face-6 ((t (:foreground "dark slate blue"))))
     (gnus-cite-face-7 ((t (:foreground "dark slate grey"))))
     (gnus-cite-face-8 ((t (:foreground "dark slate blue"))))
     (gnus-cite-face-9 ((t (:foreground "dark slate grey"))))
     (gnus-cite-face-10 ((t (:foreground "dark slate blue"))))
     (gnus-cite-face-11 ((t (:foreground "dark slate grey"))))
     (gnus-emphasis-bold ((t (:bold t :weight bold))))
     (gnus-emphasis-bold-italic
      ((t (:italic t :bold t :slant italic :weight bold))))
     (gnus-emphasis-highlight-words
      ((t (:background "black" :foreground "yellow"))))
     (gnus-emphasis-italic ((t (:italic t :slant italic))))
     (gnus-emphasis-underline ((t (:underline t :family "fixed"))))
     (gnus-emphasis-underline-bold
      ((t (:bold t :underline t :family "fixed" :weight bold))))
     (gnus-emphasis-underline-bold-italic
      ((t (:italic t :bold t :underline t :slant italic :weight bold))))
     (gnus-emphasis-underline-italic
      ((t (:italic t :underline t :slant italic :family "fixed"))))
     (gnus-group-mail-1-empty-face
      ((t (:foreground "gray24" :family "modd"))))
     (gnus-group-mail-1-face ((t (:foreground "cyan4" :family "modd"))))
     (gnus-group-mail-2-empty-face
      ((t (:foreground "gray24" :family "modd"))))
     (gnus-group-mail-2-face
      ((t (:foreground "dark slate grey" :family "modd"))))
     (gnus-group-mail-3-empty-face
      ((t (:foreground "gray24" :family "modd"))))
     (gnus-group-mail-3-face ((t (:foreground "grey" :family "modd"))))
     (gnus-group-mail-low-empty-face ((t (:foreground "grey" :family "modd"))))
     (gnus-group-mail-low-face ((t (:foreground "grey" :family "modd"))))
     (gnus-group-news-1-empty-face
      ((t (:foreground "gray24" :family "modd"))))
     (gnus-group-news-1-face ((t (:foreground "cyan4" :family "modd"))))
     (gnus-group-news-2-empty-face
      ((t (:foreground "gray24" :family "modd"))))
     (gnus-group-news-2-face ((t (:foreground "grey" :family "modd"))))
     (gnus-group-news-3-empty-face
      ((t (:foreground "gray24" :family "modd"))))
     (gnus-group-news-3-face
      ((t (:foreground "dark slate grey" :family "modd"))))
     (gnus-group-news-4-empty-face
      ((t (:foreground "Aquamarine" :family "modd"))))
     (gnus-group-news-4-face ((t (:foreground "Aquamarine" :family "modd"))))
     (gnus-group-news-5-empty-face
      ((t (:foreground "MediumAquamarine" :family "modd"))))
     (gnus-group-news-5-face
      ((t (:foreground "MediumAquamarine" :family "modd"))))
     (gnus-group-news-6-empty-face
      ((t (:foreground "MediumAquamarine" :family "modd"))))
     (gnus-group-news-6-face
      ((t (:foreground "MediumAquamarine" :family "modd"))))
     (gnus-group-news-low-empty-face ((t (:foreground "MediumAquamarine"))))
     (gnus-group-news-low-face
      ((t (:bold t :foreground "MediumAquamarine" :weight bold))))
     (gnus-header-content-face
      ((t (:foreground "dark slate blue" :family "Helvetica"))))
     (gnus-header-from-face
      ((t (:foreground "slate blue" :family "Helvetica"))))
     (gnus-header-name-face
      ((t (:foreground "dark slate gray" :family "Helvetica"))))
     (gnus-header-newsgroups-face
      ((t (:foreground "dark slate grey" :family "Helvetica"))))
     (gnus-header-subject-face
      ((t (:foreground "slate blue" :family "Helvetica"))))
     (gnus-signature-face ((t (:foreground "Grey20" :family "modd"))))
     (gnus-splash-face ((t (:foreground "ForestGreen"))))
     (gnus-summary-cancelled-face ((t (:foreground "GoldenRod"))))
     (gnus-summary-high-ancient-face
      ((t (:bold t :foreground "MediumAquamarine" :weight bold))))
     (gnus-summary-high-read-face
      ((t (:bold t :foreground "Aquamarine" :weight bold))))
     (gnus-summary-high-ticked-face
      ((t (:bold t :foreground "slate grey" :weight bold))))
     (gnus-summary-high-unread-face
      ((t (:italic t :bold t :foreground "beige" :slant italic :weight bold))))
     (gnus-summary-low-ancient-face
      ((t (:italic t :foreground "DimGray" :slant italic))))
     (gnus-summary-low-read-face ((t (:foreground "slate gray"))))
     (gnus-summary-low-ticked-face ((t (:foreground "dark slate grey"))))
     (gnus-summary-low-unread-face ((t (:foreground "LightGray"))))
     (gnus-summary-normal-ancient-face ((t (:foreground "MediumAquamarine"))))
     (gnus-summary-normal-read-face ((t (:foreground "dark slate grey"))))
     (gnus-summary-normal-ticked-face ((t (:foreground "IndianRed"))))
     (gnus-summary-normal-undownloaded-face ((t (:foreground "#666"))))
     (gnus-summary-selected-face
      ((t (:foreground "dark slate grey" :underline t))))
     (header-line
      ((t (:box (:line-width -1 :style released-button)
                :background "grey5" :foreground "grey40" :box nil))))
     (highlight ((t (:background "dark slate blue" :foreground "light blue"))))
     (highline-face ((t (:background "DeepSkyBlue4"))))
     (html-helper-italic-face ((t (:foreground "slate blue" :family "modd"))))
     (html-helper-bold-face ((t (:bold t :weight bold))))
     (html-tag-face ((t (:foreground "dark slate grey"))))
     (holiday-face ((t (:background "slate grey"))))
     (info-header-node ((t (:bold t :weight bold))))
     (info-header-xref
      ((t (:bold t :weight bold :foreground "dark slate grey"))))
     (info-menu-5 ((t (:underline t :family "fixed"))))
     (info-menu-header ((t (:bold t :family "terminus" :weight bold))))
     (info-node ((t (:bold t :weight bold))))
     (info-xref ((t (:bold t :foreground "slate grey" :weight bold))))
     (isearch ((t (:background "slate blue"))))
     (isearch-lazy-highlight-face ((t (:background "paleturquoise4"))))
     (italic ((t (:foreground "dark slate blue" :family "modd"))))
     (jde-java-font-lock-doc-tag-face ((t (:foreground "dark slate blue"))))
     (jde-java-font-lock-link-face ((t (:underline t :family "modd"))))
     (jde-java-font-lock-modifier-face ((t (:foreground "SlateBlue3"))))
     (jde-java-font-lock-package-face ((t (:foreground "grey40"))))
     (makefile-space-face ((t (:background "grey"))))
     (menu ((t (:background "grey30" :foreground "Grey"))))
     (message-cited-text-face ((t (:foreground "slate grey"))))
     (message-header-cc-face ((t (:foreground "light cyan"))))
     (message-header-name-face ((t (:foreground "LightBlue"))))
     (message-header-newsgroups-face
      ((t (:italic t :bold t :foreground "MediumAquamarine"
                   :slant italic :weight bold))))
     (message-header-other-face ((t (:foreground "MediumAquamarine"))))
     (message-header-subject-face
      ((t (:bold t :foreground "light cyan" :weight bold))))
     (message-header-to-face
      ((t (:bold t :foreground "light cyan" :weight bold))))
     (message-header-xheader-face ((t (:foreground "MediumAquamarine"))))
     (message-mml-face ((t (:foreground "ForestGreen"))))
     (message-separator-face ((t (:foreground "chocolate"))))
     (mmm-cleanup-submode-face ((t (:background "Slate Blue"))))
     (mmm-code-submode-face ((t (:background "LightGray"))))
     (mmm-comment-submode-face ((t (:background "SkyBlue"))))
     (mmm-declaration-submode-face ((t (:background "Aquamarine"))))
     (mmm-default-submode-face ((t (:background "gray85"))))
     (mmm-init-submode-face ((t (:background "gray50"))))
     (mmm-output-submode-face ((t (:background "Plum"))))
     (mmm-special-submode-face ((t (:background "MediumSpringGreen"))))
     (mode-line
      ((t (:background "grey7" :foreground "grey50"
                       :box (:line-width 1 :style nil) :family "modd"))))
     (modeline-buffer-id ((t (:background "grey50" :foreground "grey7"))))
     (modeline-mousable
      ((t (:background "slate blue" :foreground "light cyan"))))
     (modeline-mousable-minor-mode
      ((t (:background "slate blue" :foreground "slate blue"))))
     (muse-link-face ((t (:foreground "slate grey" :family "modd"))))
     (muse-link ((t (:foreground "slate grey" :family "modd"))))
     (nxml-attribute-local-name-face ((t (:foreground "cadet blue"))))
     (nxml-attribute-value-delimiter-face ((t (:foreground "#cadet blue"))))
     (nxml-attribute-value-face ((t (:foreground "#999"))))
     (nxml-comment-content-face ((t (:foreground "#777" :family "modd"))))
     (nxml-comment-delimiter-face ((t (:foreground "#777" :family "modd"))))
     (nxml-element-local-name-face ((t (:foreground "SlateBlue2"))))
     (nxml-tag-delimiter-face ((t (:foreground "SlateBlue2"))))
     (planner-cancelled-task-face 
      ((t (:foreground "dim gray" :strike-through t))))
     (planner-completed-task-face 
      ((t (:foreground "dim gray" :strike-through t))))
     (planner-id-face ((((class color) (background light)) 
                        (:foreground "slate gray"))))
     (planner-medium-priority-task-face ((t (:foreground "dark green"))))
     (quack-about-face ((t (:family "Helvetica"))))
     (quack-about-title-face
      ((t (:bold t :foreground "#00f000" :weight bold
                 :height 240 :family "Helvetica"))))
     (quack-banner-face ((t (:family "Helvetica"))))
     (quack-pltfile-dir-face
      ((t (:bold t :background "gray33" :foreground "white"
                 :weight bold :height 1.2 :family "Helvetica"))))
     (quack-pltfile-file-face
      ((t (:bold t :background "gray66" :foreground "black"
                 :weight bold :height 1.2 :family "Helvetica"))))
     (quack-pltfile-prologue-face
      ((t (:background "gray66" :foreground "black"))))
     (quack-pltish-class-defn-face ((t (:foreground "dark slate grey"))))
     (quack-pltish-comment-face ((t (:foreground "#555" :family "modd"))))
     (quack-pltish-defn-face ((t (:foreground "dark slate grey"))))
     (quack-pltish-keyword-face
      ((t (:foreground "dark slate blue" :family "fixed"))))
     (quack-pltish-module-defn-face ((t (:foreground "purple1"))))
     (quack-pltish-paren-face ((t (:foreground "gray40"))))
     (quack-pltish-selfeval-face ((t (:foreground "dark slate blue"))))
     (quack-smallprint-face ((t (:height 80 :family "Courier"))))
     (quack-threesemi-h1-face ((t (:height 1.4 :family "Helvetica"))))
     (quack-threesemi-h2-face ((t (:height 1.2 :family "Helvetica"))))
     (quack-threesemi-h3-face ((t (:bold t :weight bold :family "Helvetica"))))
     (quack-threesemi-semi-face ((t (:family "modd"))))
     (quack-threesemi-text-face ((t (:family "modd"))))
     (region ((t (:background "DarkSlateBlue"))))
     (scroll-bar ((t (:background "grey75"))))
     (secondary-selection ((t (:background "steel blue"))))
     (semantic-dirty-token-face ((t (:background "gray10"))))
     (semantic-unmatched-syntax-face ((t (:underline "red"))))
     (show-paren-match-face ((t (:background "#333"))))
     (show-paren-mismatch-face ((t (:background "red" :foreground "white"))))
     (speedbar-button-face ((t (:foreground "dark slate grey"))))
     (speedbar-directory-face ((t (:foreground "slate blue"))))
     (speedbar-file-face ((t (:foreground "dark slate grey"))))
     (speedbar-highlight-face ((t (:foreground "slate blue"))))
     (speedbar-selected-face ((t (:foreground "slate grey" :underline t))))
     (speedbar-separator-face
      ((t (:background "blue" :foreground "white" :overline "gray"))))
     (speedbar-tag-face ((t (:foreground "dark slate grey"))))
     (svn-status-filename-face ((t (:foreground "dark slate grey"))))
     (svn-status-directory-face ((t (:foreground "dark slate blue"))))
     (svn-status-marked-face ((t (:foreground "cyan4"))))
     (svn-status-modified-external-face ((t (:foreground "slate blue"))))
     (tool-bar ((t (:background "grey75" :foreground "black"
                                :box (:line-width 1 :style released-button)))))
     (tooltip ((t (:background "grey60" :foreground "black"))))
     (trailing-whitespace ((t (:background "red"))))
     (underline ((t (:underline t :family "fixed"))))
     (variable-pitch ((t (:family "terminus"))))
     (w3m-anchor-face ((t (:foreground "slate grey" :family "modd"))))
     (w3m-arrived-anchor-face ((t (:foreground "dark slate grey"))))
     (w3m-bold-face ((t (:family "modd" :foreground "slate blue"))))
     (w3m-current-anchor-face ((t (:underline t :family "modd"))))
     (w3m-form-button-face
      ((t (:background "grey20" :foreground "slate grey"
                       :box (:line-width 1 :style nil)))))
     (w3m-form-button-mouse-face
      ((t (:background "DarkSeaGreen1" :foreground "black"
                       :box (:line-width 2 :style released-button)))))
     (w3m-form-button-pressed-face
      ((t (:background "grey20" :foreground "lightyellow"
                       :box (:line-width 2 :style pressed-button)))))
     (w3m-form-face
      ((t (:foreground "slate grey" :underline t :family "fixed"))))
     (w3m-header-line-location-content-face
      ((t (:background "grey5" :foreground "dark slate grey"))))
     (w3m-header-line-location-title-face
      ((t (:background "grey5" :foreground "dark slate blue"))))
     (w3m-history-current-url-face ((t (:underline t :family "fixed"))))
     (w3m-image-face ((t (:foreground "grey60"))))
     (w3m-tab-background-face ((t (:background "grey5" :foreground "gray"))))
     (w3m-tab-selected-face
      ((t (:background "grey5" :foreground "dark slate blue"
                       :box (:line-width 1 :style nil)))))
     (w3m-tab-selected-retrieving-face
      ((t (:background "grey5" :foreground "dark slate blue"
                       :box (:line-width 1 :style nil)))))
     (w3m-tab-unselected-face
      ((t (:background "grey5" :foreground "#555"
                       :box (:line-width 1 :style nil)))))
     (w3m-tab-unselected-retrieving-face
      ((t (:background "Gray5" :foreground "#555"
                       :box (:line-width 1 :style nil)))))
     (w3m-underline-face ((t (:underline t :family "fixed"))))
     (widget-button-face ((t (:bold t :weight bold))))
     (widget-button-pressed-face ((t (:foreground "red"))))
     (widget-documentation-face ((t (:foreground "grey"))))
     (widget-field-face ((t (:background "gray30" :foreground "grey"))))
     (widget-inactive-face ((t (:foreground "gray70"))))
     (widget-single-line-field-face
      ((t (:background "dim grey" :foreground "slate blue"))))
     (woman-bold-face
      ((t (:bold t :foreground "dark slate grey"
                 :weight bold :family "fixed"))))
     (woman-italic-face ((t (:foreground "cyan4"))))
     (woman-unknown-face ((t (:foreground "slate grey"))))
     (zmacs-region ((t (:background "DarkSlateBlue")))))))
