;; .emacs file
;; Guillermo Navarro

;;; local dirs:
;;
;; configuration stuff -> ~/etc/emacs/config
;; custom config files -> ~/etc/emacs/config/custom
;; elisp packages      -> ~/etc/emacs/site-lisp
;; documents dir       -> ~/Documents
;; local info          -> ~/Documents/info
;;
(defconst g-emacs-dir "~/etc/emacs/config/"
  "Main Emacs configuration directory" )
(defconst g-custom-dir (expand-file-name "custom/" g-emacs-dir)
  "My Custom Emacs configuration files")
(defconst local-lisp-dir "~/etc/emacs/site-lisp"
  "Local directory with elisp packages")
(defconst g-docs-dir "~/Documents" "Local docs directory")
(defconst g-local-info-dir "~/Documents/info" "Local info directory")

(add-to-list 'load-path local-lisp-dir)
(add-to-list 'load-path g-custom-dir)

;;; load subdirs from local-lisp-dir if its not already in the
;;; load-path. (jao)
(defun g-load-path (subdir)
  (let ((path (expand-file-name subdir local-lisp-dir)))
    (when (and (file-exists-p path)
	       (not (member path load-path)))
      (add-to-list `load-path path))))

;; set enironment PATH variable and exec-path
(defun g-exec-path (file)
  (let ((fn (expand-file-name file)))
    (add-to-list `exec-path fn nil)
    (setenv "PATH" (concat fn ":" (getenv "PATH")))))

;; set environmet INFOPATH variable
(defun g-add-info-path (path)
  (setenv "INFOPATH" (concat path ":" (getenv "INFOPATH"))))

;; set info path
;;; local info path
(g-add-info-path g-local-info-dir)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
;; (add-to-list 'package-archives
;;              '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("org" . "https://orgmode.org/elpa/") t)
(package-initialize)

;; use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;; diminish for use-package
(use-package diminish :ensure t)

;;custom pkgs names
(defvar g-custom-pkgs
  (mapcar (lambda (f) (intern (substring f 0 -3)))
          (directory-files g-custom-dir nil "g-.+\\.el$")))

;; load custom pkgs (function taken from jao)
(defun g-load-custom-pkgs ()
  (interactive)
  (mapc #'(lambda (pkg)
           (message 
            (format "Loading %s... %s." 
                    pkg
                    (condition-case err
                        (if (require pkg nil t) "ok" "not found")
                      (error (error-message-string err))))))
        g-custom-pkgs))

(g-load-custom-pkgs)

;; emacs' custom config's file
(setq custom-file (expand-file-name "emacs-custom.el" g-emacs-dir))
(load custom-file)

(setenv "EDITOR" "emacsclient")

;; midnight mode
(require 'midnight)
;; run it every 3 hours
(setq midnight-period 10800) ;; (eq (* 3 60 60) "3 hours")
;; clean-buffer-list: set expiration date of all buffers: 1 day (default 3 days)
(setq clean-buffer-list-delay-general 1)

;; Fix bug
;; https://www.reddit.com/r/emacs/comments/cdei4p/failed_to_download_gnu_archive_bad_request/
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=34341
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3") 


;; recompile spaceline
(if (featurep 'spaceline)
    (spaceline-compile))

;; https://github.com/purcell/exec-path-from-shell
(use-package exec-path-from-shell
  :ensure t
  :config
  (when (daemonp)
    (exec-path-from-shell-initialize)))

