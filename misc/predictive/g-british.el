(provide 'g-british)
(require 'dict-tree)
(defvar g-british nil "Dictionary g-british.")
(setq g-british '(DICT "g-british" nil t nil nil (TSTREE [nil [[nil [nil [nil [nil [nil [nil [nil [nil [nil [nil [nil [nil [nil [nil (2) nil nil] nil 110] nil 111] nil 105] nil 116] nil 97] nil 115] nil 105] nil 114] nil 111] nil 104] nil 116] nil 117] [nil [nil [nil [nil [nil [nil [nil [nil [nil [nil [nil [nil (0) nil nil] nil 100] nil 101] nil 116] nil 117] nil 98] nil 105] nil 114] nil 116] nil 115] nil 105] nil 100] 97] [nil [nil [nil [nil [nil [nil [nil [nil (0) nil nil] nil 104] nil 99] nil 114] nil 97] nil 101] nil 115] nil 101] [[nil [nil [nil [nil [nil [nil [nil [nil (0) nil nil] nil 115] nil 109] nil 101] nil 116] nil 115] nil 121] nil 115] [nil [nil [nil (1) nil nil] nil 101] nil 104] nil 116] 114] nil t] (lambda (a b) (cond ((and (null a) (null b)) 0) ((null a) -1) ((null b) 1) (t (- a b)))) (lambda (new cell) (if (null cell) (dictree--wrap-data (funcall (lambda (weight data) (cond ((not (or weight data)) 0) ((null weight) (1+ data)) ((null data) weight) (t (+ weight data)))) new nil)) (dictree--set-data cell (funcall (lambda (weight data) (cond ((not (or weight data)) 0) ((null weight) (1+ data)) ((null data) weight) (t (+ weight data)))) new (dictree--get-data cell))) cell)) lambda (a b) (funcall (lambda (a b) (if (= (cdr a) (cdr b)) (if (= (length (car a)) (length (car b))) (string< (car a) (car b)) (< (length (car a)) (length (car b)))) (> (cdr a) (cdr b)))) (cons (car a) (dictree--get-data (cdr a))) (cons (car b) (dictree--get-data (cdr b))))) (lambda (new cell) (if (null cell) (dictree--wrap-data (funcall (lambda (weight data) (cond ((not (or weight data)) 0) ((null weight) (1+ data)) ((null data) weight) (t (+ weight data)))) new nil)) (dictree--set-data cell (funcall (lambda (weight data) (cond ((not (or weight data)) 0) ((null weight) (1+ data)) ((null data) weight) (t (+ weight data)))) new (dictree--get-data cell))) cell)) (lambda (a b) (funcall (lambda (a b) (if (= (cdr a) (cdr b)) (if (= (length (car a)) (length (car b))) (string< (car a) (car b)) (< (length (car a)) (length (car b)))) (> (cdr a) (cdr b)))) (cons (car a) (dictree--get-data (cdr a))) (cons (car b) (dictree--get-data (cdr b))))) nil nil nil nil nil 0.1))
(let ((ordered-hash (make-hash-table :test 'equal))
      (tstree (dictree--tstree g-british)))
  (mapc
   (lambda (entry)
     (puthash
      (car entry)
      (dictree--cache-create
       (mapcar
        (lambda (key)
          (cons key (tstree-member tstree key)))
        (dictree--cache-completions (cdr entry)))
       (dictree--cache-maxnum (cdr entry)))
      ordered-hash))
   (dictree--ordered-hash g-british))
  (dictree--set-ordered-hash g-british ordered-hash))
(dictree--set-filename g-british (locate-library "g-british"))
(unless (memq g-british dictree-loaded-list) (push g-british dictree-loaded-list))
