(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("fa2b58bb98b62c3b8cf3b6f02f058ef7827a8e497125de0254f56e373abee088" default)))
 '(package-selected-packages
   (quote
    (symon bm dired-subtree w3m jinja2-mode elpy smex whole-line-or-region which-key web-mode use-package undo-tree tango-plus-theme sr-speedbar spacemacs-theme spaceline solidity-mode rainbow-mode projectile paredit org-bullets magit js2-mode helm go-mode git-timemachine git-ps1-mode geiser flycheck flx fill-column-indicator expand-region ess eshell-z eshell-prompt-extras ebib dsvn dired+ diff-hl counsel company-tern company-auctex biblio auctex-latexmk anzu)))
 '(safe-local-variable-values (quote ((TeX-master . cv)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
 
