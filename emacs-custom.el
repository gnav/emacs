(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(zotxt zenburn-theme writegood-mode whole-line-or-region which-key web-mode w3m vterm visual-fill-column use-package undo-tree synosaurus sr-speedbar sphinx-doc spacemacs-theme solidity-mode smex smart-dash smart-comment rainbow-mode rainbow-delimiters python-docstring peep-dired paredit org-web-tools org-static-blog org-ref org-noter org-download org-bullets ob-translate moe-theme markdown-mode magit langtool json-mode js2-refactor indent-guide hl-todo highlight-indent-guides guess-language go-projectile git-timemachine gift-mode geiser flycheck flx fish-completion expand-region exec-path-from-shell excorporate eww-lnum ess eshell-up eshell-prompt-extras esh-autosuggest erc-hl-nicks elpy elfeed-org ein ebib dsvn diredfl dired-sidebar dired-recent dired-narrow dired-git-info dired-avfs diminish diff-hl dashboard cython-mode counsel company-statistics company-reftex company-quickhelp company-go company-auctex color-theme-sanityinc-tomorrow circe cider calfw-org calfw-cal calfw bufler bm blacken auctex-latexmk artbollocks-mode anzu all-the-icons-ibuffer all-the-icons-dired aggressive-indent ace-window academic-phrases)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
 
