;; gnome stuff


;; add to gnome recent files list
;; http://www.florian-diesch.de/doc/emacs/add-to-gnomes-recently-used-documents/
;; needs the 'addtorecent' script from http://www.florian-diesch.de/software/python-scripts/dist/addtorecent
;(defun fd-add-file-to-recent ()
;  (when buffer-file-name
;    (start-process "addtorecent" nil "addtorecent"
;                   (concat "file://" buffer-file-name)
;                   "text/plain"
;                   "Emacs"
;                   "emacsclient %F")))
;
;(add-hook 'find-file-hook 'fd-add-file-to-recent)

(provide 'g-gnome)
