;; haskell config

;; use haskell-mode for .xmobarrc file
(setq auto-mode-alist
      (cons '(".xmobarrc" . haskell-mode) auto-mode-alist))

;; enable haskell-indentation
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)


(provide 'g-haskell)
