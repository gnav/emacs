;; w3m config
;; guille


(when (require 'w3m-load nil t)
  (require 'w3m)

  (setq w3m-command "w3m")
  (setq w3m-use-tab t)
  (setq w3m-use-cookies t)
  (setq w3m-cookie-accept-bad-cookies t)
  (setq w3m-icon-directory (expand-file-name "emacs-w3m/icons/"
                                             local-lisp-dir))
  (setq w3m-default-save-directory "~/Downloads")
  (setq browse-url-browser-function 'w3m-browse-url)

  ;; make sure we propertly quit w3m when quitting emacs
  (add-hook 'kill-emacs-hook '(lambda () (w3m-quit t)))

  ;; download with w3m-wget
  (when (require 'w3m-wget nil t)
    (add-hook 'w3m-mode-hook '(lambda () (require 'w3m-wget)))
    (setq wget-download-directory "~/Downloads"))

  ;; Search
  (require 'w3m-search)
  (setq w3m-search-default-engine "google")
  (setq w3m-search-engine-alist
        (append
         '(("wordreference-es-en"
            "http://www.wordreference.com/es/translation.asp?tranword=%s&dict=esen&B=Buscar"
            nil)
           ("wordreference-en-es"
            "http://www.wordreference.com/es/translation.asp?tranword=%s&dict=enes&B=Buscar"
            nil)
           ("merriam-webster"
            "http://www.m-w.com/cgi-bin/dictionary?book=Dictionary&va=%s&x=0&y=0"
            nil)
           ("merriam-webster-th"
            "http://www.m-w.com/cgi-bin/dictionary?book=Thesaurus&va=%s&x=0&y=0"
            nil)
           ("rae"
            "http://buscon.rae.es/draeI/SrvltGUIBusUsual?TIPO_HTML=2&TIPO_BUS=3&LEMA=%s"
            nil)
           ("emacswiki" "http://www.emacswiki.org/cgi-bin/wiki/%s" nil)
           ("grec" ;;Gran diccionari de la llengua catalana
            "http://ec.grec.net/cgi-bin/AppDLC.exe?APP=CERCADLC&GECART=%s&x=37&y=14"
            nil))
         w3m-search-engine-alist))


  (setq w3m-uri-replace-alist
        (append
         '(("\\`deb:" w3m-search-uri-replace "debian-pkg-testing")
           ("\\`es:"  w3m-search-uri-replace "wordreference-en-es")
           ("\\`en:"  w3m-search-uri-replace "wordreference-es-en")
           ("\\`mw:"  w3m-search-uri-replace "merriam-webster")
           ("\\`th:"  w3m-search-uri-replace "merriam-webster-th")
           ("\\`rae:" w3m-search-uri-replace "rae")
           ("\\`cat:"  w3m-search-uri-replace "grec")
           ("\\`emacs:" w3m-search-uri-replace "emacswiki"))
         w3m-uri-replace-alist))

  ;; (autoload 'w3m-link-numbering-mode "w3m-lnum" nil t)
  ;; (add-hook 'w3m-mode-hook 'w3m-link-numbering-mode)

  ;; (defun jao-w3m-go-to-linknum ()
  ;;   "Turn on link numbers and ask for one to go to."
  ;;   (interactive)
  ;;   (let ((active w3m-link-numbering-mode))
  ;;     (when (not active) (w3m-link-numbering-mode))
  ;;     (unwind-protect
  ;;         (w3m-move-numbered-anchor (read-number "Anchor number: "))
  ;;       (when (not active) (w3m-link-numbering-mode)))))
  
  ;; (define-key w3m-mode-map "f" 'jao-w3m-go-to-linknum)


;;   ;; Use ido for mode line switching (M. Olson)
;;   (defun w3m-e21-switch-to-buffer ()
;;     "Run `id-switch-buffer' and redisplay the header-line.
;; Redisplaying is done by wobbling the window size."
;;     (interactive)
;;     (prog1
;;         (ido-switch-buffer)
;;       (when (and header-line-format
;;                  (eq major-mode 'w3m-mode))
;;         (w3m-force-window-update))))


;;; remove ^M on textareas (Joaquin)
  (defadvice w3m-form-input-textarea-mode 
    (after w3m-cleaning-textarea activate)
    "Cleaning the ^M when we edit a textarea from w3m"
    (interactive)
    (while (re-search-forward "\r\n" nil t)
      (replace-match "\n" nil nil)))

  ;; return the current page link and name
  (defun g-w3m-link ()
    (interactive)
    (let ((wb (w3m-alive-p)))
      (when wb
        (let ((url (with-current-buffer wb w3m-current-url))
              (title (w3m-buffer-title wb)))
          (cons url title)))))


)

(provide 'g-w3m)


