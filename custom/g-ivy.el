;; ivy, counsel, swiper
;; https://github.com/abo-abo/swiper
;;

(use-package flx
  :ensure t)

;; use swiper for search
(use-package swiper
  :ensure t
  :bind
  (("C-s" . swiper)
   ("C-c u" . swiper-all)))

(use-package ivy
  :ensure t
  :diminish ivy-mode
  :config
  (setq ivy-use-virtual-buffers t)

  (setq ivy-re-builders-alist '((ivy-switch-buffer . ivy--regex-fuzzy)
                                (completion-at-point . ivy--regex-fuzzy)
                                (t . ivy--regex-plus)))
  
  (setq ivy-count-format "(%d/%d)")
  (ivy-mode 1)
  ;; select current input with C-p (do not complete)
  (setq ivy-use-selectable-prompt t))
 
(use-package counsel
  :ensure t
  :diminish counsel-mode
  :custom
  (counsel-find-file-at-point t)
  :config
  (counsel-mode))

;; smex is needed to make counsel-M-x predictive
(use-package smex :ensure t)

(provide 'g-ivy)
