;; ivy, counsel, swiper
;; https://github.com/abo-abo/swiper

(use-package flx
  :ensure t)

;; use swiper for search
(use-package swiper
  :ensure t
  :bind
  (("C-s" . swiper)
   ("C-c u" . swiper-all)))


(use-package ivy
  :ensure t
  :diminish ivy-mode
  :init
  (setq ivy-use-virtual-buffers t)
  (setq ivy-re-builders-alist '((ivy-switch-buffer . ivy--regex-fuzzy)
                                (t . ivy--regex-plus)))
  (setq ivy-count-format "(%d/%d)")
  (ivy-mode 1)
  ;; select current input with C-p (do not complete)
  (setq ivy-use-selectable-prompt t))

(use-package counsel
  :ensure t
  :bind (("M-x" . counsel-M-x)
         ("C-x C-f" . counsel-find-file)
         ("C-h f" . counsel-describe-function)
         ("C-h v" . counsel-describe-variable)
         ("M-y" . counsel-yank-pop))
  :init
  (setq counsel-find-file-at-point t))

;; smex is needed to make counsel-M-x predictive
(use-package smex :ensure t)

(provide 'g-ivy)
