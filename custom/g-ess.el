;;ESS 


(use-package ess-site
  :ensure ess
  :mode ("\\.R\\'" . R-mode)
  :commands R)

;; (when (require 'ess-site nil t)
;;   ;;enable rdired
;;   (autoload 'ess-rdired "ess-rdired"
;;     "View *R* objects in a dired-like buffer." t)

;;   (require 'ess-rutils)
  
;;   (add-hook 'ess-mode-hook
;;             (lambda ()
;;               (ess-set-style 'GNU 'quiet)))
  
;;   (add-hook 'inferior-ess-mode-hook
;;             '(lambda nil
;;                (define-key inferior-ess-mode-map [\C-up]
;;                  'comint-previous-matching-input-from-input)
;;                (define-key inferior-ess-mode-map [\C-down]
;;                  'comint-next-matching-input-from-input)
;;                (define-key inferior-ess-mode-map [\C-x \t]
;;                  'comint-dynamic-complete-filename))))

(provide 'g-ess)
