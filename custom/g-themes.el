;;
;; Nice themes:
;; - zenburn: https://github.com/bbatsov/zenburn-emacs
;; - modus: https://protesilaos.com/modus-themes/
;; - spacemacs: https://github.com/nashamri/spacemacs-theme

(use-package zenburn-theme
  :ensure t
  :config
  (load-theme 'zenburn t)
  ;; flat mode-line
  (set-face-attribute 'mode-line nil :box nil)
  (set-face-attribute 'mode-line-inactive nil :box nil))

(provide 'g-themes)
