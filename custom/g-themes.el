;;
;; emacs 24 themes config
;;

(use-package spacemacs-theme
  :ensure t
  :defer t
  :init
  (load-theme 'spacemacs-light t t)
  (load-theme 'spacemacs-dark t t)

  ;; different themes for termnial and graphical frames
  ;; based on http://lists.gnu.org/archive/html/help-gnu-emacs/2012-02/msg00237.html
  (defun mb/pick-color-theme (frame)
    (select-frame frame)
    (if (window-system frame)
        (progn
          ;; graphical
          (disable-theme 'spacemacs-dark)
          (enable-theme 'spacemacs-light))
      (progn
        ;; terminal 
        (disable-theme 'spacemacs-light) 
        (enable-theme 'spacemacs-dark))))
  (add-hook 'after-make-frame-functions 'mb/pick-color-theme)

  ;; When started with emacs or emacs -nw rather than emacs --daemon
  (if (display-graphic-p)
      (enable-theme 'spacemacs-light)
    (enable-theme 'spacemacs-dark)))

(use-package tango-plus-theme :ensure t)


(provide 'g-themes)
