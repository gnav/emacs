;; EasyPG config
;; guille

(require 'auth-source)
(setq auth-sources
      '((:source "~/.emacs.d/authinfo.gpg")))

(use-package epa-file
  :config
  (epa-file-enable)
  (setq epa-file-cache-passphrase-for-symmetric-encryption t))

(defun g-private-get-credentials (server)
  (let ((match (car (auth-source-search :host server :max 1))))
    (if match
        (let ((secret (plist-get match :secret))
              (user (plist-get match :user)))
          (list user (when secret (funcall secret)))))))

(defun g-private-get-user (server)
  (car (g-private-get-credentials server)))

(defun g-private-get-secret (server)
  (cadr (g-private-get-credentials server)))

(provide 'g-epg)
