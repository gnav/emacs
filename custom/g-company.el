
(use-package company
  :ensure t
  :defer t
  :diminish company-mode
  :init
  (global-company-mode)
  (setq company-show-numbers t))

(use-package company-statistics
  :ensure t
  :init
  (company-statistics-mode))

(use-package company-quickhelp
  :ensure t
  :config
  (company-quickhelp-mode))

(provide 'g-company)
