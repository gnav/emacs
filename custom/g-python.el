;; LSP dependencies:
;;   pip install python-language-server pyls-black pyls-mypy pyls-isort

(use-package python
  :after lsp-mode
  :hook
  (python-mode . lsp-deferred)

  :config
  ;; enable flake8 pluing for lsp
  (setq lsp-pyls-plugins-flake8-enabled t)

  ;; pyls plugins for lsp
  (lsp-register-custom-settings
   '(("pyls.plugins.pyls_mypy.enabled" t t)
     ("pyls.plugins.pyls_mypy.live_mode" nil t)
     ("pyls.plugins.pyls_black.enabled" t t)
     ("pyls.plugins.pyls_isort.enabled" t t)

     ;; Disable these as they're duplicated by flake8
     ("pyls.plugins.pycodestyle.enabled" nil t)
     ("pyls.plugins.mccabe.enabled" nil t)
     ("pyls.plugins.pyflakes.enabled" nil t)
     ))
  )

(use-package pyvenv
  :ensure t
  :config
  (pyvenv-mode 1))

(use-package pip-requirements :ensure t)


(use-package smart-dash
  :ensure t
  :hook (python-mode . smart-dash-mode))

;; (use-package cython-mode
;;   :ensure t)

;; (use-package sphinx-doc
;;   :ensure t
;;   :hook (python-mode . sphinx-doc-mode))

;; (use-package python-docstring
;;   :ensure t
;;   :custom (python-docstring-sentence-end-double-space nil)
;;   :hook (python-mode . python-docstring-mode))

;; Jupyter notebooks
;; https://github.com/millejoh/emacs-ipython-notebook
(use-package ein
  :ensure t
  :commands (ein:notebooklist-open))

;; sagemath
;; TODO need to set --simple-prompt option when calling sage
(use-package sage-shell-mode
  :ensure t
  :hook
  (sage-shell-mode . eldoc-mode)
  (sage-shell:sage-mode .eldoc-mode)

  :init
  (sage-shell:define-alias))

;; org-babel integration https://github.com/sagemath/ob-sagemath
(use-package ob-sagemath
  :after org
  :ensure t
  :custom
  (org-babel-default-header-args:sage '((:session . t)
                                        (:results . "output"))))

(provide 'g-python)
