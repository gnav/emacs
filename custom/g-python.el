;; Python config
;; guille

(use-package flycheck
  :ensure t)

(use-package elpy
  :ensure t
  :config
  (elpy-enable)
  (setq elpy-modules (delete 'elpy-module-highlight-indentation elpy-modules))
  (setq elpy-rpc-backend "jedi")
  (setq elpy-remove-modeline-lighter nil)
  (setq elpy-rpc-python-command "python3")
  (setq python-shell-interpreter "python3"
        python-shell-interpreter-args "-i")

  ;; use flycheck
  (when (require 'flycheck nil t)
    (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
    (add-hook 'elpy-mode-hook 'flycheck-mode)))

(provide 'g-python)
