
(use-package irfc
  :ensure t
  :config
  (progn
    (setq irfc-directory (expand-file-name "RFC" g-docs-dir))
    (setq irfc-assoc-mode t)
  
    ;; use default face for keywords (MUST, SHOULD, ...)
    (set-face-attribute 'irfc-requirement-keyword-face nil :weight 'unspecified 
                        :foreground 'unspecified)))
  
(provide 'g-irfc)
