
(use-package avy
  :ensure t
  :bind ("C-c SPC" . avy-goto-word-1))


(provide 'g-avy)
