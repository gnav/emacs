
;; Keyboard (global keybindings)

;; Keyboard input method
(setq default-input-method "catalan-prefix")
;; (setq default-input-method "spanish-prefix")

(global-set-key (kbd "<f10>") 'dired-jump)
(global-set-key (kbd "<f9>") 'eshell-toggle)

;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; unbind C-z (suspend-frame)
(global-unset-key (kbd "C-z"))

(use-package which-key
  :ensure t
  :config (which-key-mode)
  :diminish ((which-key-mode . "")))

(provide 'g-kbd)


