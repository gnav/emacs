;; Keyboard (global keybindings)
;; keybindings for specific modes are usually defined in its use-package

;; Keyboard input method
;; (setq default-input-method "spanish-prefix")
;; (setq default-input-method "latin-1-prefix")
(setq default-input-method "catalan-prefix")

(global-set-key (kbd "<f10>") 'dired-jump)
(global-set-key (kbd "<f9>") 'eshell-toggle)

;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; unbind C-z (suspend-frame)
(global-unset-key (kbd "C-z"))

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :config (which-key-mode))

;; hydra
(use-package hydra
  :ensure t)

(defhydra hydra-move
  (:body-pre (next-line))
  "move"
  ("n" next-line)
  ("p" previous-line)
  ("f" forward-char)
  ("b" backward-char)
  ("a" beginning-of-line)
  ("e" move-end-of-line)
  ("v" scroll-up-command)
  ("V" scroll-down-command)
  ("l" recenter-top-bottom))
(global-set-key (kbd "C-n") #'hydra-move/next-line)


(defhydra hydra-goto-line (goto-map ""
                                    :pre (linum-mode 1)
                                    :post (linum-mode -1))
  "goto-line"
  ("g" goto-line "go")
  ("m" set-mark-command "mark" :bind nil)
  ("q" nil "quit"))

;; org motion (https://ericjmritz.wordpress.com/2015/10/14/some-personal-hydras-for-gnu-emacs/)
(defhydra hydra-org-motion (:color red :columns 3)
  "Org Mode Movements"
  ("n" outline-next-visible-heading "next heading")
  ("p" outline-previous-visible-heading "prev heading")
  ("N" org-forward-heading-same-level "next heading at same level")
  ("P" org-backward-heading-same-level "prev heading at same level")
  ("u" outline-up-heading "up heading")
  ("g" org-goto "goto" :exit t))
(define-key org-mode-map (kbd "C-c C-n")
  #'hydra-org-motion/outline-next-visible-heading)


(defhydra hydra-resize-window (:color red :columns 3)
  "Resize windows"
  ("<left>" shrink-window-horizontally "-narrower-")
  ("<right>" enlarge-window-horizontally "-wider-")
  ("<down>" shrink-window "|shorter|")
  ("<up>" enlarge-window "|longer|")
  ("=" balance-windows "equal")
  ("q"  nil))
(global-set-key (kbd "C-c #") #'hydra-resize-window/body)


(provide 'g-kbd)


