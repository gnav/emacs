;; eww config


(use-package eww
  :defer t
  :bind (("C-x m" . 'browse-url-at-point))
  :init
  ;; eww as default browser
  ;;(setq browse-url-browser-function 'eww-browse-url)
  
  ;; use firefox as external browser
  (setq browse-url-secondary-browser-function 'browse-url-generic)
  (setq browse-url-generic-program (executable-find "firefox"))

  (defun g-open-eww ()
    (interactive)
    (condition-case nil
        (browse-url-at-point)
      (error (eww))))
  
  (use-package eww-lnum
    :ensure t
    :config
    (bind-key "f" #'eww-lnum-follow eww-mode-map)
    (bind-key "U" #'eww-lnum-universal eww-mode-map))
  )


(provide 'g-eww)
