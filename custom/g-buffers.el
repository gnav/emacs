

(use-package bufler
  :ensure t
  :bind (("C-x C-b" . #'bufler-list)
         ("C-x b" . #'bufler-switch-buffer)))

(provide 'g-buffers)
