;; ibuffer config


(global-set-key (kbd "C-x C-b") 'ibuffer)
(autoload 'ibuffer "ibuffer" "List buffers." t)

(setq ibuffer-saved-filter-groups
      '(("default"
         ("dired" (mode . dired-mode))
         ("files" (filename . ".*"))
         ("org" (or
                 (mode . diary-mode)
                 (mode . org-mode)
                 (mode . org-agenda-mode)))
         ("mail" (or
                  (mode . message-mode)
                  (mode . bbdb-mode)
                  (mode . mail-mode)
                  (mode . gnus-group-mode)
                  (mode . gnus-summary-mode)
                  (mode . gnus-article-mode)
                  (name . "^\\(\\.bbdb\\|dot-bbdb\\)$")
                  (name . "^\\.newsrc-dribble$")
                  (mode . newsticker-mode)))
         ("doc" (or
                 (mode . Info-mode)
                 (mode . apropos-mode)
                 (mode . woman-mode)
                 (mode . help-mode)
                 (mode . Man-mode))))))

(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-auto-mode 1)
            (ibuffer-switch-to-saved-filter-groups "default")
            ;; Override the ibuffer-find-file function to use helm
            (define-key ibuffer-mode-map (kbd "C-x C-f")
              (lambda ()
                (interactive)
                (let ((default-directory (let ((buf (ibuffer-current-buffer)))
                                           (if (buffer-live-p buf)
                                               (with-current-buffer buf
                                                 default-directory)
                                             default-directory))))
                  (helm-find-files-1 default-directory))))))



(provide 'g-ibuffer)
