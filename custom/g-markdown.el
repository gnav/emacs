
(use-package markdown-mode
  :ensure t
  :init (dolist (ext '("\\.md$" "\\.markdown$"))
          (add-to-list 'auto-mode-alist (cons ext 'markdown-mode))))

(provide 'g-markdown)
