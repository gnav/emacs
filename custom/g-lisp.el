;; lisp, scheme and co.
;; guille


(use-package geiser
  :ensure t)

(use-package paredit
  :ensure t
  :diminish paredit-mode " þ"
  :config
  (add-hook 'scheme-mode-hook     #'enable-paredit-mode)
  (add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode)
  (add-hook 'clojure-mode-hook    #'enable-paredit-mode)
  (add-hook 'lisp-mode-hook #'enable-paredit-mode)
  (eval-after-load "paredit"
    #'(define-key paredit-mode-map (kbd "C-j") 'eval-last-sexp)))

  

(provide 'g-lisp)
