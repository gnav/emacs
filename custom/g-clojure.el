;; clojure config

(use-package paredit
  :ensure t)

(use-package clojure-mode
  :ensure t
  :config (progn (add-hook 'clojure-mode-hook #'paredit-on)))

(use-package cider
  :ensure t
  :commands cider-mode
  :config (progn (add-hook 'cider-mode-hook #'company-mode)
                 (add-hook 'cider-mode-hook #'eldoc-mode)))

(provide 'g-clojure)
