;; eshell
;; Guillermo Navarro

(require 'eshell)

;; eshel aliases file
(setq eshell-aliases-file (expand-file-name "misc/eshell.alias" g-emacs-dir))

;; interactive remove
(setq eshell-rm-interactive-query t)

;; history
(setq eshell-buffer-maximum-lines 20000)
(setq eshell-history-size 350)

;;;; browse html file
(defun eshell/browse (file)
  (w3m (concat "file://" (expand-file-name file)) t))

(defun eshell/infof (file)
  "Read an info file in an info buffer."
  (Info-find-node (expand-file-name file) "Top"))

(require 'esh-toggle)

;;;###autoload
(defun g-goto-eshell (arg)
  (interactive "P")
  (if arg
      (eshell-toggle-cd)
    (eshell-toggle nil)))

(use-package eshell-autojump :ensure t :disabled t)

(use-package eshell-z
  :ensure t
  :init (with-eval-after-load 'eshell (require 'eshell-z)))

(use-package eshell-prompt-extras
  :ensure t
  :config
  (with-eval-after-load "esh-opt"
    (autoload 'epe-theme-lambda "eshell-prompt-extras")
    (setq eshell-highlight-prompt nil
          eshell-prompt-function 'epe-theme-lambda)))

;; http://www.howardism.org/Technical/Emacs/eshell-fun.html
(defun eshell-here ()
  "Opens up a new shell in the directory associated with the
 current buffer's file. The eshell is renamed to match that
 directory to make multiple eshell windows easier."
  (interactive)
  (let* ((parent (if (buffer-file-name)
                     (file-name-directory (buffer-file-name))
                   default-directory))
         (height (/ (window-total-height) 3))
         (name (car (last (split-string parent "/" t))))
         (ename (concat "*eshell: " name "*")))
    (split-window-vertically (- height))
    (other-window 1)
    (if (get-buffer ename)
        (switch-to-buffer ename nil t)
      (eshell "new")
      (rename-buffer ename)
      (insert "ls")
      (eshell-send-input))))

(defun eshell/x ()
  (let ((b (current-buffer)))
    (delete-window)
    (with-current-buffer b (eshell/exit))))

(provide 'g-eshell)
