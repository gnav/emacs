(use-package solidity-mode
  :ensure t
  :defer t
  :config
  (setq solidity-solc-path  "/home/guille/bin/"))

(provide 'g-solidity)
