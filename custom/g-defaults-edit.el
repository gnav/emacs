;;; generic editing stuff
;;;
;;; Time-stamp: <2020-11-28 19:11:37 guille>

;;backups
(setq version-control t)
(setq backup-by-copying t)
(setq backup-directory-alist (quote (("." . "~/.emacs.d/backups"))))
(setq delete-old-versions t)
(setq kept-new-versions 5)
(setq kept-old-versions 2)

;; org-mode is the default major mode
(setq-default major-mode 'org-mode)

;; kill whole line if at the beg.
(setq kill-whole-line t)

;; spaces and tabs
(setq-default indent-tabs-mode nil)
(setq indent-tabs-width 4)

;; sentences separated by just one space
(setq sentence-end "[.?!][]\"')]*\\($\\|\t\\| \\)[ \t\n]*")
(setq sentence-end-double-space nil)

;; show wrapped lines in visual-line-mode
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

;; autofill at 80
(setq-default fill-column 80)
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; tab: indent or complete
(setq tab-always-indent 'complete)

;; save place in each file (file defaults to ~/.emacs.d/places)
(save-place-mode 1)

;; ediff
(add-hook 'ediff-before-setup-hook 'new-frame)
(add-hook 'ediff-quit-hook 'delete-frame)
;; (setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-vertically)

;; copy rectangle
(defun kill-rectangle-save (start end)
  "Save the region-rectangle as the last killed one."
  (interactive "r")
  (require 'rect)           ; Make sure killed-rectangle is defvar'ed.
  (setq killed-rectangle (extract-rectangle start end))
  (message "Rectangle saved"))

;; unfill paragraph with its second call (two M-q)
;; http://endlessparentheses.com/fill-and-unfill-paragraphs-with-a-single-key.html
(defun endless/fill-or-unfill ()
  "Like `fill-paragraph', but unfill if used twice."
  (interactive)
  (let ((fill-column
         (if (eq last-command 'endless/fill-or-unfill)
             (progn (setq this-command nil)
                    (point-max))
           fill-column)))
    (call-interactively #'fill-paragraph)))

(global-set-key [remap fill-paragraph] #'endless/fill-or-unfill)

(defun g-yank-as-comment ()
  "yank and comment"
  (interactive)
  (save-excursion
    (let ((beg (point)))
      (yank)
      (comment-region beg (point)))))
(global-set-key (kbd "C-c y") 'g-yank-as-comment)

;; indent after yanking for some modes
(defadvice yank (after indent-region activate)
  (let ((mark-even-if-inactive t))
    (if (member major-mode 
                '(emacs-lisp-mode scheme-mode lisp-mode
                                  LaTeX-mode TeX-mode))
        (indent-region (region-beginning) (region-end) nil))))


;; time stamps: update time stamp template if present in the first 8 lines of a
;; file as Time-stamp: <> or Time-stamp: " "
(add-hook 'before-save-hook 'time-stamp) ; update when saving

(use-package visual-fill-column
  :ensure t
  :config
  (add-hook 'visual-fill-column-mode-hook #'visual-line-mode))

(use-package aggressive-indent
  :ensure t
  :config
  (global-aggressive-indent-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'web-mode))

(use-package highlight-indent-guides
  :ensure t
  :diminish highlight-indent-guides-mode
  :config
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-responsive 'top)
  (setq highlight-indent-guides-auto-character-face-perc 5)
  (add-hook 'prog-mode-hook 'highlight-indent-guides-mode))

;; rainbow delimiters 
(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

;; multiple-cursors
(use-package multiple-cursors
  :bind (("C-S-c C-S-c" . mc/edit-lines)
         ("C-S-c ." . mc/mark-all-like-this)))

;; whole-line-or-region (act on current line if no region is active)
(use-package whole-line-or-region
  :ensure t
  :diminish whole-line-or-region-local-mode 
  :config
  (whole-line-or-region-global-mode))

;; anzu (for query/replace, etc.)
(use-package anzu
  :ensure t
  :bind (("M-%" . anzu-query-replace)
         ("C-M-%" . anzu-query-replace-regexp))
  :config
  (global-anzu-mode t)
  (setq anzu-mode-lighter ""))

;; https://github.com/tarsius/hl-todo
(use-package hl-todo
  :ensure t
  :hook ((latex-mode . hl-todo-mode)
         (LaTeX-mode . hl-todo-mode)
         (prog-mode . hl-todo-mode)))

;; show color codes as the actual code
(use-package rainbow-mode
  :ensure t
  :defer t)

(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region))

(use-package hippie-expand
  :bind ("M-/" . hippie-expand))

;; GIFT format for quizes (moodle)
(use-package gift-mode
  :ensure t)


(provide 'g-defaults-edit)
