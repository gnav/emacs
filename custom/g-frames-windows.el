;; Generic config

(setq inhibit-startup-message t)
(setq visible-bell t)

;; mode line
(line-number-mode 1)
(column-number-mode 1)

;;; default frame
(add-to-list 'default-frame-alist '(width  . 80))
(add-to-list 'default-frame-alist '(vertical-scroll-bars . nil))
(add-to-list 'default-frame-alist '(menu-bar-lines . 0))
(add-to-list 'default-frame-alist '(tool-bar-lines . 0))

(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-11"))

;; show matching parents
(require 'paren)
(show-paren-mode t)

;; change yes/no for y/n
(fset 'yes-or-no-p 'y-or-n-p)

;;use uniquify for buffer names
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

;; windows
(defun prev-window ()
  "go to previous window"
  (interactive)
  (other-window -1))

(defun swap-windows ()
  "If you have 2 windows, it swaps them. (from Steve Yegge)"
  (interactive)
  (cond ((/= (count-windows) 2)
         (message "You need exactly 2 windows to do this."))
        (t
         (let* ((w1 (first (window-list)))
                (w2 (second (window-list)))
                (b1 (window-buffer w1))
                (b2 (window-buffer w2))
                (s1 (window-start w1))
                (s2 (window-start w2)))
           (set-window-buffer w1 b2)
           (set-window-buffer w2 b1)
           (set-window-start w1 s2)
           (set-window-start w2 s1))))
  (other-window 1))

(use-package sr-speedbar
  :ensure t
  :config (setq speedbar-use-images nil))

(provide 'g-frames-windows)
