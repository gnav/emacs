;;; auctex, reftex
;;; Guillermo Navarro

(use-package tex
  :defer t
  :ensure auctex
  :config

  ;; revert pdf buffer after compilation
  (add-hook 'TeX-after-compilation-finished-functions
            #'TeX-revert-document-buffer)

  ;; use pdf-tools for view
  (setq TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view)))
  (setq TeX-view-program-selection '((output-pdf "PDF Tools")))
  (setq TeX-source-correlate-start-server t)

  ;; enable parse on load and parse on save
  (setq TeX-parse-self t)
  (setq TeX-auto-save t)
  (setq TeX-auto-local ".tex-auto-local")

  (setq TeX-master 'dwin)
  
  ;; query for master file
  (setq-default TeX-master nil)

  (use-package latex
    :defer t
    :mode ("\\.tikz$" . LaTeX-mode)
    :config
    (add-hook 'LaTeX-mode-hook #'turn-on-reftex)
    (add-hook 'LaTeX-mode-hook #'LaTeX-math-mode)
    (add-hook 'LaTeX-mode-hook #'flyspell-mode)
    (add-hook 'LaTeX-mode-hook #'TeX-source-correlate-mode)
    
    (setq LaTeX-math-menu-unicode t)))

(use-package bibtex
  :config 
  ;; bibtex-mode autokey format
  (setq bibtex-autokey-additional-names ".etal.")
  (setq bibtex-autokey-name-separator ".")
  (setq bibtex-autokey-year-length 4)
  (setq bibtex-autokey-titleword-ignore nil)
  (setq bibtex-autokey-titleword-length nil))


(use-package reftex
  :config
  (setq reftex-enable-partial-scans t)
  (setq reftex-save-parse-info t)
  (setq reftex-use-multiple-selection-buffers t)
  (setq reftex-plug-into-AUCTeX t)

  (setq reftex-default-bibliography '("~/Documents/bibliography/org-ref/references.bib"))
  
  ;;Automatically insert non-breaking space before citation
  (setq reftex-format-cite-function 
        '(lambda (key fmt)
           (let ((cite (replace-regexp-in-string "%l" key fmt)))
             (if (or (= ?~ (string-to-char fmt))
                     (member (preceding-char) '(?\ ?\t ?\n ?~ ?,)))
                 cite
               (concat "~" cite))))))

(use-package company-auctex
  :ensure t
  :config (company-auctex-init))

(use-package company-reftex
  :ensure t
  :after company
  :config
  (add-to-list 'company-backends #'company-reftex-labels)
  (add-to-list 'company-backends #'company-reftex-citations))

(use-package auctex-latexmk
  :ensure t
  :config
  (auctex-latexmk-setup)
  (setq auctex-latexmk-inherit-TeX-PDF-mode t))

(use-package ebib
  :ensure t)

(use-package biblio
  :ensure t)

(provide 'g-latex)
  
