;;; auctex, reftex
;;; Guillermo Navarro

(use-package tex
  :defer t
  :ensure auctex
  :init
  ;; revert pdf buffer after compilation
  (add-hook 'TeX-after-compilation-finished-functions
            #'TeX-revert-document-buffer)

  (setq TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view)))
  (setq TeX-view-program-selection '((output-pdf "PDF Tools")))
  (setq TeX-source-correlate-start-server t)
  ;; enable parse on load and parse on save
  (setq TeX-parse-self t)
  (setq TeX-auto-save t)
  (setq TeX-auto-local ".tex-auto-local")

  ;; query for master file
  (setq-default TeX-master nil)

  (use-package latex
    :defer t
    :mode ("\\.tikz$" . LaTeX-mode)
    :config
    (add-hook 'LaTeX-mode-hook #'turn-on-reftex)
    (add-hook 'LaTeX-mode-hook #'LaTeX-math-mode)
    (add-hook 'LaTeX-mode-hook #'flyspell-mode)
    (add-hook 'LaTeX-mode-hook #'TeX-source-correlate-mode)
    
    (setq LaTeX-math-menu-unicode t))

  ;; :config
  
  ;; (add-to-list
  ;;  'TeX-command-list
  ;;  '("View Gnome"
  ;;    "gio open %s.pdf"
  ;;    TeX-run-command
  ;;    t               ;no confirmation
  ;;    t                 ;all modes
  ;;    :help "Run external viewer (Gnome)"))  
  )

(use-package reftex
  :config
  (setq reftex-enable-partial-scans t)
  (setq reftex-save-parse-info t)
  (setq reftex-use-multiple-selection-buffers t)
  (setq reftex-plug-into-AUCTeX t)

  ;;Automatically insert non-breaking space before citation
  (setq reftex-format-cite-function 
        '(lambda (key fmt)
           (let ((cite (replace-regexp-in-string "%l" key fmt)))
             (if (or (= ?~ (string-to-char fmt))
                     (member (preceding-char) '(?\ ?\t ?\n ?~ ?,)))
                 cite
               (concat "~" cite))))))


(use-package company-auctex
  :ensure t
  :config (company-auctex-init))

(use-package auctex-latexmk
  :ensure t
  :config
  (auctex-latexmk-setup)
  (setq auctex-latexmk-inherit-TeX-PDF-mode t))

(use-package ebib
  :ensure t)

(use-package biblio
  :ensure t)

(provide 'g-latex)
  
