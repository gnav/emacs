;; mode-line config

(use-package spaceline-config
  :ensure spaceline
  :config
  (spaceline-emacs-theme)
  (setq powerline-default-separator 'bar)
  (setq spaceline-minor-modes-separator " ")
  (setq spaceline-highlight-face-func 'spaceline-highlight-face-modified)
  (spaceline-compile))

(provide 'g-mode-line)
