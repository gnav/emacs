;; Generic config

(setq inhibit-startup-message t)
(setq visible-bell t)

;; mode line
(line-number-mode 1)
(column-number-mode 1)

;;; default frame
(add-to-list 'default-frame-alist '(width  . 80))
(add-to-list 'default-frame-alist '(vertical-scroll-bars . nil))
(add-to-list 'default-frame-alist '(menu-bar-lines . 0))
(add-to-list 'default-frame-alist '(tool-bar-lines . 0))                            

;; default font
;;(add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-11"))
(add-to-list 'default-frame-alist '(font . "Iosevka-13"))

;; window title <buffer>[<mode>]
(setq-default frame-title-format '("%@ %b [%m]"))

;; fringe
(setq-default indicate-buffer-boundaries 'left)
(setq-default indicate-empty-lines +1)

;; fill-column-indicator
(global-display-fill-column-indicator-mode)

;; scroll
;; defer fontification while there is input pending (speed up scrolling)
(setq jit-lock-defer-time 0)

;; show matching parents
(require 'paren)
(show-paren-mode t)

;; highlight current line in prog and text modes
(add-hook 'prog-mode-hook #'hl-line-mode)
(add-hook 'text-mode-hook #'hl-line-mode)

;; change yes/no for y/n
(fset 'yes-or-no-p 'y-or-n-p)

;; use uniquify for buffer names
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

;; always autorevert
(use-package autorevert
  :ensure t
  :diminish t
  :hook
  (dired-mode . auto-revert-mode)
  :config
  (global-auto-revert-mode +1)
  :custom
  (auto-revert-verbose nil))

;;
;; windows
;;

(use-package ace-window
  :ensure t
  :config
  (global-set-key (kbd "M-o") 'ace-window)
  (global-set-key [remap other-window] 'ace-window))

(defun prev-window ()
  "go to previous window"
  (interactive)
  (other-window -1))

(defun swap-windows ()
  "If you have 2 windows, it swaps them. (from Steve Yegge)"
  (interactive)
  (cond ((/= (count-windows) 2)
         (message "You need exactly 2 windows to do this."))
        (t
         (let* ((w1 (first (window-list)))
                (w2 (second (window-list)))
                (b1 (window-buffer w1))
                (b2 (window-buffer w2))
                (s1 (window-start w1))
                (s2 (window-start w2)))
           (set-window-buffer w1 b2)
           (set-window-buffer w2 b1)
           (set-window-start w1 s2)
           (set-window-start w2 s1))))
  (other-window 1))


(use-package sr-speedbar
  :ensure t
  :bind ("<f7>" . sr-speedbar-toggle)
  :config
  (setq speedbar-use-images nil)
  (setq speedbar-show-unknown-files t)
  (setq speedbar-initial-expansion-list-name "quick buffers")
  ;; show all files
  (setq speedbar-directory-unshown-regexp "^$"))

;; dashboard: https://github.com/emacs-dashboard/emacs-dashboard
(use-package dashboard
  :ensure t
  :custom
  (initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
  (dashboard-items '((agenda)
                     (recents . 12)
                     (bookmarks . 12)
                     (projects . 12)))
  (dashboard-set-footer nil)
  (dashboard-startup-banner nil)
  (dashboard-week-agenda t)
  (dashboard-filter-agenda-entry 'dashboard-no-filter-agenda)
  :config
  (dashboard-setup-startup-hook))

(use-package all-the-icons :ensure t)

(provide 'g-defaults)
