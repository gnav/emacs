
(use-package undo-tree
  :ensure t
  :config (global-undo-tree-mode 1)
  :bind ("C-x u" . undo-tree-visualize))


(provide 'g-undo-tree)
