;;;;;;;;;;
;; writing / language
;;;;;;;;;;

(use-package guess-language
  :ensure t
  :defer t
  :init (add-hook 'text-mode-hook #'guess-language-mode)
  :config
  (setq guess-language-langcodes '((en . ("en_US" "English"))
                                   (es . ("es_ES" "Spanish"))
                                   (ca . ("ca_CA" "Catalan"))))
  (setq guess-language-languages '(en es ca))
  )


;; https://github.com/bnbeckwith/writegood-mode
(use-package writegood-mode
  :ensure t
  :bind ("C-c g" . writegood-mode))

;; grammar checker: LangTool
;; https://github.com/mhayashi1120/Emacs-langtool
;; https://languagetool.org
(use-package langtool
  :ensure t
  :custom
  ;; (langtool-default-language "en-US")
  (langtool-default-language "es")
  (langtool-mother-tongue "es")
  (langtool-language-tool-jar
   "/home/guille/applications/LanguageTool-4.8/languagetool-commandline.jar")
  (langtool-language-tool-server-jar
   "/home/guille/applications/LanguageTool-4.8/languagetool-server.jar"))

;; https://github.com/nashamri/academic-phrases
(use-package academic-phrases
  :ensure )

;; https://github.com/hpdeifel/synosaurus
(use-package synosaurus
  :ensure t)

;; https://github.com/sachac/artbollocks-mode
(use-package artbollocks-mode
  :ensure t
  :hook (LaTeX-mode-hook org-mode-hook))

(provide 'g-language)
