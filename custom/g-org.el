;;; org-mode
;;; guille

(use-package org
  :mode (("\\.org$" . org-mode))
  :bind (("C-c a" . org-agenda))

  :custom
  (org-directory "~/Documents/org")
  (org-special-ctrl-a/e t)
  (org-startup-indented t)
  (org-fontify-quote-and-verse-blocks t)
  (org-default-notes-file (expand-file-name "notes.org" org-directory))
  (org-agenda-include-diary t)
  
  :config
  ;; agenda / diary
  (setq org-agenda-files '(expand-file-name "agenda" org-directory))
  
  ;; babel
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((calc . t)
     (ditaa . t)
     (emacs-lisp . t)
     (latex . t)
     (R . t)
     ;;(sh . t)
     ))

  ;; add link to open in xournalpp
  (org-link-set-parameters
   "xournalpp"
   :follow (lambda (link)
             (let ((process-connection-type nil))
               (start-process "org-xournalpp" nil "/usr/bin/xournalpp" link))))

  ;; enable org-crypt
  (require 'org-crypt)
  (org-crypt-use-before-save-magic)

  ;; attachments
  (setq org-attach-id-dir "org-data"))

;; HTML exporter
(use-package ox-html
  :after org
  :custom
  (org-html-validation-link nil)
  (org-html-doctype "html5")
  (org-html-html5-fancy t))

;; Markdown exporter
(use-package ox-md :after org)

;; Latex exporter
(use-package ox-latex
  :after org
  :config
  (setq org-latex-listings 't)
  (add-to-list 'org-latex-packages-alist '("" "listings"))
  (add-to-list 'org-latex-packages-alist '("" "color")))

;; Beamer exporter
(use-package ox-beamer :after org)

;; Capture
(use-package org-capture
  :after org
  :bind ("C-c t" . org-capture)

  :preface
  ;; org-capture-extension (https://github.com/sprig/org-capture-extension)
  (defun transform-square-brackets-to-round-ones(string-to-transform)
    "Transforms [ into ( and ] into ), other chars left unchanged."
    (concat 
     (mapcar #'(lambda (c) (if (equal c ?[) ?\( (if (equal c ?]) ?\) c))) string-to-transform)))

  :custom
  (org-capture-templates
   '(("t" "TODO" entry
      (file (lambda () (expand-file-name "agenda/todo.org" org-directory)))
      "* TODO %?\n  %i\n  %a\nAdded: %U")
     ;; org-capture-extension (https://github.com/sprig/org-capture-extension)
     ("p" "Protocol" entry (file+headline ,(concat org-directory "notes.org") "Inbox")
      "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")	
     ("L" "Protocol Link" entry (file+headline ,(concat org-directory "notes.org") "Inbox")
      "* %? [[%:link][%(transform-square-brackets-to-round-ones \"%:description\")]]\n"))
   ))


(use-package org-web-tools
  :ensure t
  :demand t
  :after org
  :bind (:map org-mode-map
              ("C-c i" . org-web-tools-insert-link-for-url)))

(use-package org-bullets
  :ensure t
  :after org
  :hook (org-mode . org-bullets-mode))

(use-package org-download
  :ensure t
  :after org
  :config
  (org-download-enable))

;; https://github.com/weirdNox/org-noter
(use-package org-noter
  :ensure t
  :after org)

;; (use-package org-sticky-header
;;   :ensure t
;;   :after org
;;   :hook (org-mode . org-sticky-header-mode)
;;   :config
;;   (setq org-sticky-header-full-path 'full))

(use-package org-ref
  :ensure t
  :after org
  :init
  (setq org-ref-bibliography-notes "~/Documents/bibliography/org-ref/notes.org")
  (setq org-ref-default-bibliography '("~/Documents/bibliography/org-ref/references.bib"))
  (setq org-ref-pdf-directory "~/Documents/bibliography/org-ref/bibtex-pdfs"))

(use-package org-zotxt
  :ensure zotxt
  :after org
  :diminish (org-zotxt-mode . "Z"))

(use-package org-static-blog
  :ensure t
  :commands (org-static-blog-create-new-post
	     org-static-blog-publish
	     org-static-blog-publish-file)
  :init
  (load-file (concat g-emacs-dir "web-org.el")))

(provide 'g-org)
