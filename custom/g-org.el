;;; org-mode
;;; guille

(use-package org
  :mode (("\\.org$" . org-mode))
  :bind (("C-c l" . org-store-link)
         ("C-c r" . org-capture))
  :init

  ;; remove validation link in HTML exports
  (setq org-html-validation-link nil)
  
  (setq org-directory (expand-file-name "org" g-docs-dir)
        org-agenda-files (list (expand-file-name "agenda" org-directory))
        org-default-notes-file (expand-file-name "notes.org" org-directory)
          
        org-capture-templates
        '(("t" "Todo" entry (file+datetree "agenda/refile.org")
           "* TODO %?\n  %i%a\n (Added %u)")))
  (setq org-module '(org-babel
                     org-bibtex
                     org-docview
                     org-info
                     org-src
                     org-table
                     org-w3m))  

  ;; allow refile in all agenda files (9 levels deep)
  (setq org-refile-targets '((org-agenda-files :maxlevel . 9)))
  (setq org-refile-use-outline-path 'file)

  (setq org-agenda-restore-windows-after-quit t)

  (setq org-todo-keywords
        '((sequence "TODO(t)" "STARTED(s!)" "|"
                    "DONE(d@)" "CANCELLED(c@)")))

  ;; don't show deadline/scheduled if already scheduled or done
  (setq org-agenda-skip-deadline-prewarning-if-scheduled t)
  (setq org-agenda-skip-deadline-if-done t)
  (setq org-agenda-skip-scheduled-if-done t)

  (setq org-agenda-custom-commands
        '(("n" "Agenda and TODOs"
           ((agenda "")
            (alltodo)))
          ("h" "Agenda organized TODOs"
           ((agenda "")
            (tags-todo "teaching")
            (tags-todo "research")
            (tags-todo "misc"))
           ((org-agenda-compact-blocks t)))))
  
  ;; start agenda on current day
  (setq org-agenda-start-on-weekday nil)

  ;; enforce hierarchical dependencies
  (setq org-enforce-todo-dependencies t)

  ;; load markdown exporter
  (eval-after-load "org"
    '(require 'ox-md nil t))
  
  (use-package org-bullets
    :ensure t))

(provide 'g-org)
