
(use-package erc
  :ensure t
  :commands (erc erc-tls)
  :custom
  (erc-prompt-for-password nil)
  (erc-prompt-for-nickserv-password nil)

  (erc-rename-buffers t)
  (erc-fill-function 'erc-fill-static)
  (erc-fill-static-center 20)
  (erc-prompt ">")

  (erc-autojoin-channels-alist '(("freenode.net" "#emacs" "#org-mode")))
  (erc-autojoin-timing 'ident)

  (erc-server-reconnect-attempts 5)
  (erc-server-reconnect-timeout 3)

  :preface
  (defun g-erc ()
    (interactive)
    (erc-tls :server "chat.freenode.net" :port 6697 :nick "gnav"))

  :config
  (add-to-list 'erc-modules 'spelling)
  (erc-services-mode 1)
  (erc-update-modules))


(use-package erc-hl-nicks
  :ensure t
  :after erc)

(provide 'g-erc)
