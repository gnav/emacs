;; Ido

;; (require 'ido)

;; (setq ido-save-directory-list-file "~/.emacs.d/ido.last")
;; (setq ido-enable-flex-matching t)
;; (setq ido-use-filename-at-point t)
;; (setq ido-use-url-at-point t)
;; (setq ibuffer-show-empty-filter-groups nil)

;; (use-package flx-ido
;;   :ensure t
;;   :init (flx-ido-mode 1))

;; (use-package ido-vertical-mode
;;   :ensure t
;;   :init (ido-vertical-mode 1)
;;   :config (setq ido-vertical-define-keys 'C-n-C-p-up-down-left-right))

;; (use-package ido-hacks
;;   :ensure t)

;; ;; enable ido everywhere
;; (setq ido-everywhere t)
;; (ido-mode 1)

(provide 'g-ido)
