;; calendar stuff

;; mark current day
(add-hook 'today-visible-calendar-hook 'calendar-mark-today)

(setq european-calendar-style t)

;; week starts on monday
(setq calendar-week-start-day 1)

;; use a4 size
(add-hook 'cal-tex-hook 'my-calendar-a4)

(defun my-calendar-a4 ()
  "Replace all occurences of 18cm with 17cm."
  (goto-char (point-min))
  (while (search-forward "18cm" nil t)
    (replace-match  "17cm")))


(provide 'g-calendar)
