(use-package web-mode
  :ensure t
  :mode (("\\.html\\'" . web-mode)
         ("\\.djhtml\\'" . web-mode)
         ("\\.jinja\\'" . web-mode)
         ("\\.jinja2\\'" . web-mode)
         ("\\.php\\'" . web-mode))
  :config (setq-default web-mode-markup-indent-offset 2
                        web-mode-css-indent-offset 2
                        web-mode-code-indent-offset 2
                        web-mode-attr-indent-offset 2))

(provide 'g-web)
