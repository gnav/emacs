
(use-package projectile
  :ensure t
  :config
  (define-key projectile-mode-map (kbd "C-c C-p") 'projectile-command-map)
  (setq projectile-mode-line-prefix "P"))

(provide 'g-projectile)
