
(use-package projectile
  :ensure t
  :config
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (setq projectile-switch-project-action #'projectile-dired)
  (setq projectile-completion-system 'ivy)
  (setq projectile-mode-line-prefix "Pr")
  (setq projectile-dynamic-mode-line nil)
  (setq-default projectile--mode-line projectile-mode-line-prefix)
  (projectile-global-mode))


(provide 'g-projectile)
