;; fill column indicator

(use-package fill-column-indicator
  :ensure t
  :init
  (setq fci-rule-column 80)
  (add-hook 'LaTeX-mode-hook 'fci-mode)
  (add-hook 'clojure-mode-hook 'fci-mode)
  (add-hook 'emacs-lisp-mode-hook 'fci-mode)
  (add-hook 'ess-mode-hook 'fci-mode)
  (add-hook 'lisp-mode-hook 'fci-mode)
  (add-hook 'markdown-mode-hook 'fci-mode)
  (add-hook 'python-mode-hook 'fci-mode)
  (add-hook 'scheme-mode-hook 'fci-mode))

(provide 'g-fci)
