
(use-package symon
  :ensure t
  :config
  (setq symon-sparkline-type 'plain)
  ;; (symon-mode)
  )

(provide g-symon)
