;; elfeed
;;
;; https://github.com/skeeto/elfeed
;;


(use-package elfeed
  :ensure t
  :bind
  (("<f5>" . elfeed))
  :config
  (setq elfeed-db-directory (expand-file-name "elfeed" user-emacs-directory)))


(use-package elfeed-org
  :after (elfeed)
  :ensure t
  :init
  (elfeed-org)
  :config
  (setq rmh-elfeed-org-files
        (list (expand-file-name "elfeed.org" g-emacs-dir))))



(provide 'g-elfeed)
