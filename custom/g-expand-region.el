
(use-package expand-region
  :ensure t
  :bind ("C-=" . er/expand-region)
  :config
  (eval-after-load "flyspell"
    ;; disable C-. keybinding from flyspell
    '(define-key flyspell-mode-map (kbd "C-.") nil)))


(provide 'g-expand-region)
