
(use-package yasnippet
  :ensure t
  :config
  (progn
    ;; my snippets dir
    (setq g-snippet-dir (expand-file-name "snippets" g-emacs-dir))
    (setq yas-snippet-dirs g-snippet-dir)
  
    ;; load snippets
    ;(yas-reload-all)
  
    ;; use ido or completing prompt (avoid default x prompt)
    (setq yas-prompt-functions '(yas/ido-prompt
                                 yas/completing-prompt))

    ;; enable yasnippet for some major modes
    (add-hook 'clojure-mode-hook    '(lambda () (yas-minor-mode)))
    (add-hook 'emacs-lisp-mode-hook '(lambda () (yas-minor-mode)))
    (add-hook 'LaTeX-mode-hook      '(lambda () (yas-minor-mode)))
    (add-hook 'python-mode-hook     '(lambda () (yas-minor-mode)))
    
    ;; indent whole snippet after expansion
    (add-hook 'yas-after-exit-snippet-hook
              '(lambda ()
                 (indent-region yas/snippet-beg yas/snippet-end)))
  ))

(provide 'g-yasnippet)
