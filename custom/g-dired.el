;;; dired
;;; -guille-


(use-package dired
  :ensure nil
  :config
  (setq dired-listing-switches "-alFhD --group-directories-first")

  ;; guess target directory 
  (setq dired-dwim-target t)

  ;; delete files to trash
  (setq delete-by-moving-to-trash t)

  ;; wdired
  (setq wdired-allow-to-change-permissions t)
  (add-hook 'dired-mode-hook
            (lambda ()
              (local-set-key (kbd "C-c C-r")
                             'wdired-change-to-wdired-mode)))

  ;; highlight line in dired
  (add-hook 'dired-after-readin-hook #'hl-line-mode)

  ;; load dired-x
  (require 'dired-x)

  ;; shell guessing (dired-x)
  (setq dired-guess-shell-alist-user
        '(("\\.odt\\'\\|\\.ods\\'\\|\\.odp\\'\\|\\.odg\\'" "libreoffice")
          ("\\.docx\\'\\|\\.doc\\'" "libreoffice")
          ("\\.xlsx\\'\\|\\.xls\\'" "libreoffice")
          ("\\.pptx\\'\\|\\.ppt\\'" "libreoffice")
          ("\\.pdf\\'" "setsid -w xdg-open")
          ("\\.ps\\'" "setsid -w xdg-open")
          ("\\.eps\\'" "setsid -w xdg-open")
          ("\\.dvi\\'" "setsid -w xdg-open")))
  
  ;; hide all dot files except "." and ".." in dired-omit-mode (dired-x)
  (setq dired-omit-files "^\\...+$")
  (define-key dired-mode-map (kbd "C-c o") 'dired-omit-mode))

;; dired-recent https://github.com/Vifon/dired-recent.el
;; use C-x C-d to select a recent dir in dired
(use-package dired-recent
  :ensure t
  :config
  (dired-recent-mode 1))

;; dired-subtree (from https://github.com/Fuco1/dired-hacks)
(use-package dired-subtree
  :ensure t
  :bind (:map dired-mode-map
              ("i" . dired-subtree-insert)
              ("I" . dired-subtree-remove))
  :config
  (setq dired-subtree-use-backgrounds nil))

;; dired-avfs (https://github.com/Fuco1/dired-hacks#dired-avfs)
(use-package dired-avfs
  :ensure t)

;; dired-narrow (from https://github.com/Fuco1/dired-hacks)
(use-package dired-narrow
  :after dired
  :ensure t
  :bind (:map dired-mode-map
              ("/" . dired-narrow)))

;; peep-dired (https://github.com/asok/peep-dired)
(use-package peep-dired
  :ensure t
  :defer t
  :bind (:map dired-mode-map ("P" . peep-dired)))

;; https://github.com/purcell/diredfl
;; add font lock colors (inspired in dired+)
(use-package diredfl
  :ensure t
  :config
  (diredfl-global-mode))

;; https://github.com/clemera/dired-git-info
(use-package dired-git-info
  :ensure t
  :bind (:map dired-mode-map (")" . dired-git-info-mode)))

;; (use-package dired-sidebar
;;   :ensure t
;;   :bind (("C-x C-n" . dired-sidebar-toggle-sidebar))
;;   :commands (dired-sidebar-toggle-sidebar)
;;   )

(defun g-rename-file-with-timestamp ()
  "Rename the file at point by adding a timestamp of the form
\"%Y%m%d.\" at the begining of the filename."
  (interactive)
  (let ((file (dired-get-filename 'no-dir))
        (timestamp (format-time-string "%Y%m%d")))
    (rename-file file (concat timestamp "." file) 1)
    (revert-buffer)))

;; ediff two marked files
;; https://oremacs.com/2017/03/18/dired-ediff/
;; -*- lexical-binding: t -*-
(defun ora-ediff-files ()
  (interactive)
  (let ((files (dired-get-marked-files))
        (wnd (current-window-configuration)))
    (if (<= (length files) 2)
        (let ((file1 (car files))
              (file2 (if (cdr files)
                         (cadr files)
                       (read-file-name
                        "file: "
                        (dired-dwim-target-directory)))))
          (if (file-newer-than-file-p file1 file2)
              (ediff-files file2 file1)
            (ediff-files file1 file2))
          (add-hook 'ediff-after-quit-hook-internal
                    (lambda ()
                      (setq ediff-after-quit-hook-internal nil)
                      (set-window-configuration wnd))))
      (error "no more than 2 files should be marked"))))
(eval-after-load "dired"
  '(progn
     (define-key dired-mode-map (kbd "e") 'ora-ediff-files)))

;; gnome utils
(defun gnome-open-file (filename)
  "Call gio open on filename."
  (let ((process-connection-type nil))
    (start-process "" nil "/usr/bin/gio open" filename)))

(defun g-dired-gnome-open ()
  "Calls gnome-open on current file in dired"
  (interactive)
  (gnome-open-file (dired-get-file-for-visit)))

(defun g-dired-nautilus ()
  "Open nautilus on current directory."
  (interactive)
  (let ((process-conection-type nil))
    (start-process "" nil "/usr/bin/nautilus" (dired-current-directory))))


(provide 'g-dired)
