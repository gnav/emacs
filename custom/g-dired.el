;;; dired, dired-x
;;; -guille-

(setq dired-listing-switches "-alFhD --group-directories-first")

;; guess target directory
(setq dired-dwim-target t)

;; delete files to trash
(setq delete-by-moving-to-trash t)

;; wdired
(setq wdired-allow-to-change-permissions t)
(add-hook 'dired-mode-hook
          (lambda ()
            (local-set-key (kbd "C-c C-r")
                           'wdired-change-to-wdired-mode)))

;; shell guessing (dired-x)
(setq dired-guess-shell-alist-user
      '(("\\.odt\\'\\|\\.ods\\'\\|\\.odp\\'" "xdg-open")
        ("\\.docx\\'\\|\\.doc\\'" "xdg-open")
        ("\\.xlsx\\'\\|\\.xls\\'" "xdg-open")
        ("\\.pptx\\'\\|\\.ppt\\'" "xdg-open")
        ("\\.pdf\\'" "xdg-open")
        ("\\.ps\\'" "xdg-open")
        ("\\.dvi\\'" "xdg-open")))

(use-package dired+
  :ensure t
  :config
  (setq diredp-hide-details-initially-flag nil)
  (setq diredp-hide-details-last-state nil)
  (setq diredp-hide-details-propagate-flag t))

(use-package dired-subtree
  :after dired
  :ensure t
  :bind (:map dired-mode-map
              ("i" . dired-subtree-insert)
              (";" . dired-subtree-remove))
  :config
  (setq dired-subtree-use-backgrounds nil))

(use-package dired-narrow
  :ensure t
  :bind (:map dired-mode-map
              ("/" . dired-narrow)))

(use-package peep-dired
  :ensure t
  :defer t
  :bind (:map dired-mode-map ("P" . peep-dired)))

(defun g-rename-file-with-timestamp ()
  "Rename the file at point by adding a timestamp of the form
\"%Y%m%d.\" at the begining of the filename."
  (interactive)
  (let ((file (dired-get-filename 'no-dir))
        (timestamp (format-time-string "%Y%m%d")))
    (rename-file file (concat timestamp "." file) 1)
    (revert-buffer)))

;; ediff two marked files
;; https://oremacs.com/2017/03/18/dired-ediff/
;; -*- lexical-binding: t -*-
(defun ora-ediff-files ()
  (interactive)
  (let ((files (dired-get-marked-files))
        (wnd (current-window-configuration)))
    (if (<= (length files) 2)
        (let ((file1 (car files))
              (file2 (if (cdr files)
                         (cadr files)
                       (read-file-name
                        "file: "
                        (dired-dwim-target-directory)))))
          (if (file-newer-than-file-p file1 file2)
              (ediff-files file2 file1)
            (ediff-files file1 file2))
          (add-hook 'ediff-after-quit-hook-internal
                    (lambda ()
                      (setq ediff-after-quit-hook-internal nil)
                      (set-window-configuration wnd))))
      (error "no more than 2 files should be marked"))))
(define-key dired-mode-map "e" 'ora-ediff-files)


;; gnome utils
(defun gnome-open-file (filename)
  "Call gnome-open on filename."
  (let ((process-connection-type nil))
    (start-process "" nil "/usr/bin/gio open" filename)))

(defun g-dired-gnome-open ()
  "Calls gnome-open on current file in dired"
  (interactive)
  (gnome-open-file (dired-get-file-for-visit)))

(defun g-dired-nautilus ()
  "Open nautilus on current directory."
  (interactive)
  (let ((process-conection-type nil))
    (start-process "" nil "/usr/bin/nautilus" (dired-current-directory))))


(provide 'g-dired)
