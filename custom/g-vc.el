;; Version control 


;; magit
(use-package magit
  :ensure t
  :init
  (setq magit-status-buffer-switch-function 'switch-to-buffer)
  (setq magit-status-show-hashes-in-headers t)
  :bind ("<f8>" . magit-status))

;; highlight uncommitted changes on the fringe
(use-package diff-hl
  :ensure t
  :config (progn
            (setq diff-hl-side 'right)
            (add-hook 'LaTeX-mode-hook 'turn-on-diff-hl-mode)
            (add-hook 'prog-mode-hook 'turn-on-diff-hl-mode)
            (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh)))

(use-package git-timemachine
  :ensure t)

;; subversion 
(use-package dsvn
  :ensure t
  :init (progn
          (autoload 'svn-status "dsvn" "Run `svn status'." t)
          (autoload 'svn-update "dsvn" "Run `svn update'." t)))

(provide 'g-vc)
