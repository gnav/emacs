;; helm config

(use-package helm
  :ensure t
  :bind ("C-c c" . helm-for-files)
  :init
  (progn
    (require 'helm-config)
    ;; (setq helm-completing-read-handlers-alist
    ;;       (quote 
    ;;        ((describe-function . helm-completing-read-symbols)
    ;;         (describe-variable . helm-completing-read-symbols)
    ;;         (debug-on-entry . helm-completing-read-symbols)
    ;;         (find-function . helm-completing-read-symbols)
    ;;         (find-tag . helm-completing-read-with-cands-in-buffer)
    ;;         (ffap-alternate-file . nil)
    ;;         (tmm-menubar . nil)
    ;;         ;;
    ;;         (dired-create-directory . nil)
    ;;         (execute-extended-command . nil)
    ;;         ;;org
    ;;         (org-refile . nil)
    ;;         (org-agenda-refile .nil)
    ;;         ;;auctex
    ;;         (LaTeX-environment . nil)
    ;;         (LaTeX-section . nil)
    ;;         (reftex-citation . nil)
    ;;         (TeX-command-master . nil)
    ;;         (TeX-master-file-ask . nil)
    ;;         )))
  
    ;; disable margins in scroll completion window
    (setq helm-completion-window-scroll-margin 0)
    
    (setq helm-bookmark-show-location t)))

(provide 'g-helm)
