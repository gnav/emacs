
(use-package circe
  :ensure t
  :config
  (use-package g-epg)
  ;;(setq irc-debug-log t)
  (setq circe-network-options
        `(("Freenode"
           :use-tls t
           :port 6697
           :nick ,(g-private-get-user "freenode")
           :sasl-username ,(g-private-get-user "freenode")
           :sasl-password ,(g-private-get-secret "freenode")))))


(provide 'g-circe)
