
(use-package pdf-tools
  :ensure t
  :config
  (pdf-tools-install)
  (setq-default pdf-view-display-size 'fit-page)
  
  ;; use isearch instead of swiper
  (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward)
  
  ;; disable whole-line-or-region-mode and bind M-w to pdf-view-kill-ring-save
  (add-hook 'pdf-view-mode-hook (lambda () (whole-line-or-region-local-mode -1)))
  (define-key pdf-view-mode-map (kbd "M-w") 'pdf-view-kill-ring-save)

  ;; more fine-grained zooming
  (setq pdf-view-resize-factor 1.1)

  ;; printer 
  (setq pdf-misc-print-programm "/usr/bin/lp")
  (setq pdf-misc-print-programm-args '("-o sides=two-sided-long-edge"))
  )

;; reload file if changes
(add-hook 'doc-view-mode-hook 'auto-revert-mode)

(setq doc-view-resolution 300)

(provide 'g-pdf-tools)
