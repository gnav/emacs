(deftheme g-dark
  "Created 2012-03-29, guille")

(let ((class '((class color) (min-colors 89)))
      (fore "#9c9c9c")
      (back "#050505")
      (black-1 "#000000")
      (blue-1 "#405090")
      (blue-2 "#7171F3")
      (blue-3 "#4856F7")
      (blue-7 "#4C6E7C")
      (blue-8 "#3982A0")
      (blue-9 "#2197C7")
      (blue-10 "#87ceeb")
      (cyan-1 "#50A0A0")
      (cyan-2 "#668A83")
      (brown-1 "#A2A270")
      (brown-2 "#A2925E")
      (brown-3 "#52421E")
      (grey-01 "#bbbbdd")
      (grey-0 "#aaaaaa")
      (grey-1 "#909090")
      (grey-2 "#606065")
      (grey-3 "#555753")
      (grey-4 "#404040")
      (grey-5 "#202020")
      (green-1 "#40ad40")
      (green-2 "#44836e")
      (red-1 "#ee6565")
      (red-2 "#dd3030")
      (red-3 "#703030")
      (red-4 "DarkRed")
      (yellow-1 "#b4fa70"))
  (custom-theme-set-faces
   'g-dark
   ;; frame faces
   `(default ((t (:foreground ,fore :background ,back))))
   `(cursor ((,class (:background ,green-1))))
   `(fringe ((,class (:background ,grey-5))))
   `(region ((,class (:background ,grey-4))))
   `(minibuffer-prompt ((,class (:foreground ,green-1))))
   ;; modeline
   `(mode-line ((,class (:box (:line-width 1 :color ,grey-0)
                              :background ,grey-1 :foreground ,black-1))))
   `(mode-line-inactive ((,class 
                          (:box (:line-width 1 :color ,grey-2)
                                :background ,black-1 :foreground ,grey-3))))
   ;; standard faces
   `(font-lock-keyword-face ((,class (:foreground ,cyan-1))))
   `(font-lock-builtin-face ((,class (:foreground ,blue-8))))
   `(font-lock-string-face ((,class (:foreground ,brown-1))))
   `(font-lock-doc-face ((,class (:foreground ,brown-2))))
   `(font-lock-comment-face ((,class (:foreground ,brown-2))))
   `(link ((,class (:foreground ,cyan-1 :underline t))))
   `(match ((,class (:background ,blue-1))))
   ;; specific fonts
   `(eshell-ls-backup ((,class (:foreground ,grey-2))))
   `(eshell-ls-directory ((,class (:inherit font-lock-builtin-face 
                                   :foreground unspecified 
                                   :weight unspecified))))
   `(eshell-ls-executable ((,class (:foreground unspecified :weight unspecified 
                                    :inherit warning))))
   `(eshell-prompt ((,class (:foreground ,green-1))))
   `(font-latex-sectioning-5-face ((,class (:inherit variable-pitch 
                                                     :foreground ,blue-10 
                                                     :weight unspecified))))
   `(font-latex-verbatim-face ((,class (:inherit fixed-pitch :height 0.9
                                                 :foreground unspecified))))
   `(gnus-cite-1 ((,class (:foreground ,grey-2))))
   `(gnus-cite-2 ((,class (:foreground ,grey-3))))
   `(gnus-cite-3 ((,class (:foreground ,grey-4))))
   `(gnus-cite-attribution ((,class (:foreground ,grey-01 :slant unspecified))))
   `(gnus-group-mail-3-empty ((,class (:foreground ,blue-8))))
   `(gnus-group-mail-3 ((,class (:foreground ,blue-10 :weight bold))))
   `(gnus-header-content ((,class (:foreground ,green-1 :slant unspecified))))
   `(gnus-header-name ((,class (:foreground ,green-2))))
   `(gnus-signature ((,class (:foreground ,grey-1 :slant unspecified))))
   `(gnus-summary-normal-read ((,class (:foreground ,blue-8))))
   `(helm-selection ((,class (:background ,brown-3))))
   `(helm-ff-directory ((,class (:background ,grey-5
                                 :foreground ,cyan-1))))
   `(magit-diff-none ((,class (:background ,grey-5))))
   `(outline-1 ((,class (:weight bold :foreground ,blue-10))))
   `(outline-2 ((,class (:weight bold :foreground ,blue-9))))
   `(outline-3 ((,class (:foreground ,blue-8 :inherit unspecified))))
   `(outline-4 ((,class (:foreground ,blue-7 :inherit unspecified))))
   `(org-date ((,class (:foreground ,cyan-2))))
   `(show-paren-match ((,class (:background ,blue-1))))
   `(w3m-anchor ((,class (:foreground ,cyan-1))))
   `(w3m-arrived-anchor ((,class (:foreground ,cyan-2))))
   `(w3m-image-anchor ((,class (:background ,red-3))))
   `(w3m-tab-background ((,class (:foreground ,grey-2 :background ,back))))
   `(w3m-tab-selected ((,class (:background ,grey-1 
                                            :foreground ,black-1))))
   `(w3m-tab-selected-retrieving  ((,class (:background ,grey-1 
                                            :foreground ,red-2))))
   `(w3m-tab-unselected ((,class (:background ,black-1 :foreground ,grey-3))))
   `(w3m-tab-unselected-retrieving ((,class (:background ,black-1 
                                             :foreground ,red-1))))
   `(w3m-tab-unselected-unseen ((,class (:background ,black-1 
                                         :foreground ,grey-4))))
   ))


(provide-theme 'g-dark)

;; Local Variables:
;; no-byte-compile: t
;; End:
